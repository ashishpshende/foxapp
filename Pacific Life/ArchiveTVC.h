//
//  ArchiveTVC.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 1/06/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseManager.h"
#import "ArchiveClientCell.h"
#import "MBProgressHUD.h"
#import "RequestManager.h"
@interface ArchiveTVC : UIViewController <SWTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *theTableView;
@property (weak, nonatomic) IBOutlet UILabel *clientName;
@property (nonatomic,strong) NSArray *clients;
@end
