//
//  TouchListCell.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Touch.h"
#import "ActionSheetPicker.h"
#import "DatabaseManager.h"
#import "AppConstants.h"
#import "RequestManager.h"
#import "MBProgressHUD.h"
#import "SWTableViewCell.h"

@protocol TouchListCellDelegate <NSObject>
@optional
-(void) touchChanged;
-(void) reloadDataFromLocalDatabase;
@end

@interface TouchListCell : SWTableViewCell
@property id<TouchListCellDelegate> tableDelegate;
@property (weak, nonatomic) IBOutlet UILabel *touchNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *iconLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityLabel;
@property (weak, nonatomic) IBOutlet UIImageView *accessoryImage;
@property (weak, nonatomic) IBOutlet UIButton *moreInfoButton;

@property (strong,nonatomic) NSDateFormatter *dateFormatter;
@property (strong,nonatomic) Touch *touch;
@property (nonatomic, strong) NSDate *selectedDate;
-(void)setupCellForTouch:(Touch*)touch andFormatter:(NSDateFormatter*)formatter;
- (IBAction)showMoreTouchesAction:(id)sender;
- (IBAction)changeDateAction:(id)sender;
- (IBAction)changeTextAction:(id)sender;


/*
 @property (nonatomic,strong) UILongPressGestureRecognizer *longPressSeeTouches;
 @property (nonatomic,strong) UILongPressGestureRecognizer *longPressChangeDates;
 @property (nonatomic,strong) UILongPressGestureRecognizer *longPressChangeTexts;

 */
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *showMoreTouchesButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *changeDateButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *changeTextButtons;

@end
