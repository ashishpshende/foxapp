//
//  ClientListCell.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "ClientListCell.h"
#import "DatabaseManager.h"
@implementation ClientListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupCellForTouch:(Touch*)touch andIndexPath:(NSIndexPath*)indexPath
{
    self.nameLabel.text = touch.client.name;
    self.iconLabel.text = [self touchNameForTouchType:touch.type];
    
    NSLog(@"%ld",touch.type);
    if(touch.currentColor==Red)//(hours <= 24)
    {
        [self.iconImage setImage:[self imageForTouchType:touch.type isWhite:YES]];
        [self setLighterContent];
    }
    else if(touch.currentColor==Yellow)//((hours > 24)&&(hours<(24*7)))
    {
        [self setDarkerContent];
        [self.iconImage setImage:[self imageForTouchType:touch.type isWhite:NO]];
    }
    else if(touch.currentColor==Green)//if(hours>(24*7))
    {
        [self setDarkerContent];
        [self.iconImage setImage:[self imageForTouchType:touch.type isWhite:NO]];
    }
   /*
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
    [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
    title:@"Delete"];
    self.rightUtilityButtons = rightUtilityButtons;
    */
    
    self.bigButton.tag = indexPath.row;
    self.infoButton.tag = indexPath.row;
    
}

- (IBAction)showOtherTouches:(id)sender {
    
    
    
}

-(void)setDarkerContent
{
    self.nameLabel.textColor = DarkGrayColor;
    self.iconLabel.textColor = DarkGrayColor;
    [self.accessoryImage setImage:[UIImage imageNamed:@"info_grey.png"]];
}
-(void)setLighterContent
{
    self.nameLabel.textColor = WhiteColor;
    self.iconLabel.textColor = WhiteColor;
    [self.accessoryImage setImage:[UIImage imageNamed:@"info_white.png"]];
}
-(UIImage*)imageForTouchType:(TouchType)imageType isWhite:(BOOL)isWhite
{
    
    NSString *iconName;
    switch (imageType) {
        case PhoneCall:
            iconName = @"phone_call%@.png";
            break;
        case Email:
            iconName = @"email%@.png";
            break;
        case InPersonMeeting:
            iconName = @"in_person_meeting%@.png";
            break;
        case BigBoard:
            iconName = @"custom_post%@.png";
            break;
        case MailSpecificMarketingMaterials:
            iconName = @"email%@.png";
            break;
        case FedExMail:
            iconName = @"mail%@.png";
            break;
        case Book:
            iconName = @"book%@.png";
            break;
        case ThirdPartyArticle:
            iconName = @"mailing_marketing_materials%@.png";
            break;
        case SmallGift:
            iconName = @"small_gift%@.png";
            break;
        case WebinarInvite:
            iconName = @"webinar_invite_inverted%@.png";
            break;
        case ClientInterest:
            iconName = @"activity_client_interest%@.png";
            break;
        case HandWrittenNote:
            iconName = @"thankyou_note%@.png";
            break;
        case ThankYouSummary:
            iconName = @"thankyou_note%@.png";
            break;
        case LifeEvent:
            iconName = @"birthday%@.png";
            break;
        case NewsletterDistributionList:
            iconName = @"newsletter%@.png";
            break;
        case UtilizeProspectingLetter:
            iconName = @"prospecting_letters%@.png";
            break;
        case Training:
            iconName = @"training%@.png";
            break;
        case SpecificGroupMeeting:
            iconName = @"group_meeting_followup%@.png";
            break;
        case Custom:
            iconName = @"custom_post%@.png";
            break;
        case LinkedInConnection:
            iconName = @"linkedin%@.png";
            break;
        case MarketingLetter:
            iconName =@"mailing_marketing_materials%@.png";
            break;
        case VideoConference:
            iconName =@"webinar_invite%@.png";
        default:
            iconName = @"custom_post%@.png";
            break;
    }
    if(isWhite)
    iconName =[NSString stringWithFormat:iconName,@"_white"];
    else iconName =[NSString stringWithFormat:iconName,@""];
    UIImage *image = [UIImage imageNamed:iconName];
    return image;
}

-(NSString*)touchNameForTouchType:(TouchType)touchType
{
    
    NSString *touchName;
    
    for (MasterTouch *mTouch in [[DatabaseManager instance] getAllMasterTouches])
    {
        if (mTouch.touch_id == touchType)
        {
            return mTouch.title;
        }
    }
    
    return @"CUSTOM";
    //
//    switch (touchType)
//    {
//        case PhoneCall:
//            touchName = @"PHONE CALL";
//            break;
//        case Email:
//            touchName = @"EMAIL";
//            break;
//        case InPersonMeeting:
//            touchName = @"IN PERSON MEETING";
//            break;
//        case VideoConference:
//            touchName = @"VIDEO CONFERENCE";
//            break;
//        case WebinarInvite:
//            touchName = @"WEBINAR INVITE";
//            break;
//        case FedExMail:
//            touchName = @"FED EX/MAIL";
//            break;
//        case Book:
//            touchName = @"BOOK";
//            break;
//        case ThirdPartyArticle:
//            touchName = @"THIRD PARTY ARTICLE";
//            break;
//        case SmallGift:
//            touchName = @"GIFT";
//            break;
//        case BigBoard:
//            touchName = @"BIG BOARD";
//            break;
//        case MailSpecificMarketingMaterials:
//            touchName = @"MAIL SPECIFIC MARKETING LETTER";
//            break;
//        case HandWrittenNote:
//            touchName = @"HAND WRITTEN NOTE";
//            break;
//        case LifeEvent:
//            touchName = @"LIFE EVENT";
//            break;
//        case NewsletterDistributionList:
//            touchName = @"NEWSLETTER";
//            break;
//        case UtilizeProspectingLetter:
//            touchName = @"UTILIZE PROSPECTING  LETTER";
//            break;
//        case Training:
//            touchName = @"TRAINING";
//            break;
//        case SpecificGroupMeeting:
//            touchName = @"GROUP MEETING";
//            break;
//        case ClientInterest:
//            touchName = @"ACTIVITY INTEREST";
//            break;
//        case Custom:
//            touchName = @"CUSTOM";
//            break;
//        case LinkedInConnection:
//            touchName = @"LINKEDIN CONNECTION";
//            break;
//        case ThankYouSummary:
//            touchName = @"THANK YOU";
//            break;
//        case MarketingLetter:
//            touchName = @"MARKETING LETTER";
//            break;
//        default:
//            touchName = @"CUSTOM";
//            break;
//    }
//    
    return touchName;
}
@end
