//
//  ClientListCell.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Touch.h"
#import "UIView+FrameManipulation.h"
#import "AppConstants.h"
#import "SWTableViewCell.h"
@interface ClientListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *iconLabel;
@property (weak, nonatomic) IBOutlet UIImageView *accessoryImage;
@property (weak, nonatomic) IBOutlet UIButton *bigButton;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
-(void)setupCellForTouch:(Touch*)touch andIndexPath:(NSIndexPath*)indexPath;
@end
