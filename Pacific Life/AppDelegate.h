	//
//  AppDelegate.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 14/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseManager.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
