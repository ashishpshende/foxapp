//
//  ArchiveClientCell.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 2/06/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "ArchiveClientCell.h"

@implementation ArchiveClientCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupCellForClient:(Client*)client andIndexPath:(NSIndexPath*)indexPath
{
    self.clientRowNumberLabel.text = [NSString stringWithFormat:@"%i",(int)indexPath.row+1];
    self.clientNameLabel.text = client.name;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yy"];
    self.dateLabel.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:client.updatedAt]];
    if([client.status isEqualToString:@"CW"])
    {
        [self.iconIndicatorImage setImage:[UIImage imageNamed:@"on_boarded_gray.png"]];
        [self.iconIndicatorLabel setText:@""];
    }
    else if([client.status isEqualToString:@"CL"])
    {
        [self.iconIndicatorImage setImage:[UIImage imageNamed:@"punt_gray.png"]];
        [self.iconIndicatorLabel setText:@""];
        
    }
    else
    {
        [self.iconIndicatorImage setImage:[UIImage imageNamed:@""]];
        [self.iconIndicatorLabel setText:@""];
        
    }
    //
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:11/255.0 green:54/255.0 blue:120/255.0 alpha:1.0f]
                                                title:@"Unarchive"];
    self.rightUtilityButtons = rightUtilityButtons;

}
         
- (NSString *)applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

@end
