//
//  TableRefreshControl.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 30/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableRefreshControl : UIRefreshControl

@end
