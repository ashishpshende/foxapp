//
//  Client.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "Client.h"
#import "NSDate+Utilities.h"

@implementation Client
+(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    Client *client = [Client new];
    [client populateWithDictionary:dictionary];
    return client;
}

-(void)populateWithDictionary:(NSDictionary *)dictionary{
    
    self.uuid = [dictionary valueForKey:@"id"];
    self.sellerId = [dictionary valueForKey:@"seller_id"];
    self.name = [[dictionary valueForKey:@"name"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"name"];
    self.status = [[dictionary valueForKey:@"status"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"status"];
    self.email = [[dictionary valueForKey:@"email"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"email"];
    self.company = [[dictionary valueForKey:@"company"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"company"];
    self.phone = [[dictionary valueForKey:@"phone"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"phone"];
    self.address = [[dictionary valueForKey:@"address"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"address"];
    self.state = [[dictionary valueForKey:@"state"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"state"];
    self.city = [[dictionary valueForKey:@"city"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"city"];;
    self.zip = [[dictionary valueForKey:@"zip"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"zip"];
    self.rating = [[dictionary valueForKey:@"rating"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"rating"];;
    self.isActive = [[dictionary valueForKey:@"isActive"] isKindOfClass:[NSNull class]]?NO:[[dictionary valueForKey:@"isActive"] boolValue];
    self.createdAt = [NSDate dateFromCompleteDateString:[dictionary valueForKey:@"created_at"]];
    self.updatedAt = [NSDate dateFromCompleteDateString:[dictionary valueForKey:@"updated_at"]];
}
@end
