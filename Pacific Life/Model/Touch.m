//
//  Touch.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "Touch.h"
#import"DatabaseManager.h"

@implementation Touch
@synthesize company_touchid;
+(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    Touch *touch = [Touch new];
    [touch populateWithDictionary:dictionary];
    return touch;
}

-(void)populateWithDictionary:(NSDictionary *)dictionary
{
    self.uuid = [dictionary valueForKey:@"id"];
    self.type = [[dictionary valueForKey:@"touch_id"] integerValue];
//    if(self.type==0){
//        self.customType=[[dictionary valueForKey:@"touch_id"] integerValue];
//    }
    self.client = [DatabaseManager.instance clientWithUuid:[dictionary valueForKey:@"client_id"] ];
    self.note = [[dictionary valueForKey:@"note"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"note"];
    self.completed = [[dictionary valueForKey:@"completed"] isKindOfClass:[NSNull class]]?NO:[[dictionary valueForKey:@"completed"] boolValue];
    self.date = [NSDate dateFromShortDateString:[dictionary valueForKey:@"date"]];
    self.createdAt = [NSDate dateFromCompleteDateString:[dictionary valueForKey:@"created_at"]];
    self.updatedAt = [NSDate dateFromCompleteDateString:[dictionary valueForKey:@"updated_at"]];
    self.name = [dictionary valueForKey:@"touch_name"];
    self.status = [[dictionary valueForKey:@"status"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"status"];
    self.urlLink = @"https://followupfox.com/";
    if (![[dictionary objectForKey:@"mtouch_id"] isKindOfClass:[NSNull class]])
    {
        self.mTouchId = [[dictionary objectForKey:@"mtouch_id"]integerValue];
//        self.type = self.mTouchId;
        
    }
}



// ignores the touch name to keep the one on the DB
+(instancetype)initWithUpdateDictionary:(NSDictionary *)dictionary{
    Touch *touch = [Touch new];
    [touch populateWithUpdateDictionary:dictionary];
    return touch;
}

// ignores the touch name to keep the one on the DB
-(void)populateWithUpdateDictionary:(NSDictionary *)dictionary{
    
    //[[dictionary valueForKey:@"note"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"note"];

    self.uuid = [dictionary valueForKey:@"id"];
    self.type = [[dictionary valueForKey:@"touch_id"] integerValue];
    self.client = [DatabaseManager.instance clientWithUuid:[dictionary valueForKey:@"client_id"] ];
    self.note = [[dictionary valueForKey:@"note"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"note"];
    self.completed = [[dictionary valueForKey:@"completed"] isKindOfClass:[NSNull class]]?NO:[[dictionary valueForKey:@"completed"] boolValue];
    self.date = [NSDate dateFromShortDateString:[dictionary valueForKey:@"date"]];
    self.createdAt = [NSDate dateFromCompleteDateString:[dictionary valueForKey:@"created_at"]];
    self.updatedAt = [NSDate dateFromCompleteDateString:[dictionary valueForKey:@"updated_at"]];
    self.status = [[dictionary valueForKey:@"status"]isKindOfClass:[NSNull class]]?@"":[dictionary valueForKey:@"status"];
    self.urlLink = @"https://followupfox.com/";
      if (![[dictionary objectForKey:@"mtouch_id"] isKindOfClass:[NSNull class]])
    {
        self.mTouchId = [[dictionary objectForKey:@"mtouch_id"]integerValue];
        
    }

}

@end
