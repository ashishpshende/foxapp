//
//  MasterTouch.m
//  FollowUp Fox
//
//  Created by Priya on 04/02/16.
//  Copyright © 2016 Globant. All rights reserved.
//

#import "MasterTouch.h"
#import "DatabaseManager.h"
@implementation MasterTouch
@synthesize title;
@synthesize touch_id;
@synthesize name;
@synthesize note;
@synthesize number;
@synthesize company_touchid;
-(instancetype)initWithDictionary:(NSDictionary *)dicionary
{
    self = [super init];
    if (self)
    {
       
        
        self.company_touchid =[[dicionary objectForKey:@"touch_id"] integerValue];
        if ([dicionary objectForKey:@"id"] && ![[dicionary objectForKey:@"id"] isKindOfClass:[NSNull class]] )
        {
        self.touch_id = [[dicionary objectForKey:@"id"] integerValue] ;
        }
       else
           self.touch_id = self.company_touchid;
       
        self.name = [dicionary objectForKey:@"name"];
        self.note = [dicionary objectForKey:@"note"];
        self.number = [[dicionary objectForKey:@"order_num"] integerValue];
        self.title = [self.name uppercaseString];
    }
    return self;
}
-(void) save
{
    [[DatabaseManager instance] insertMasterTouch:self];
}
@end
