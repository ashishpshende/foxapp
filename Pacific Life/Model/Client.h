//
//  Client.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Client : NSObject
@property (nonatomic,strong) NSNumber *uuid;
@property (nonatomic,strong) NSNumber *sellerId;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *company;
@property (nonatomic,strong) NSString *phone;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *state;
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *zip;
@property (nonatomic,strong) NSString *rating;
@property (nonatomic,assign) BOOL isActive;
@property (nonatomic,strong) NSDate *createdAt;
@property (nonatomic,strong) NSDate *updatedAt;
@property (nonatomic,strong) NSDate *lastTouchDate;
-(void)populateWithDictionary:(NSDictionary *)dictionary;
+(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
