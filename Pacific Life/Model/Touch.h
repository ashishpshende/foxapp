//
//  Touch.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Client.h"
#import "NSDate+Utilities.h"
@class DatabaseManager;
typedef NS_ENUM(NSInteger, TouchColor) {
    Gray = 0,
    Red = 1,
    Yellow = 2,
    Green = 3
};
/*
 typedef NS_ENUM(NSInteger, TouchType)
{
ThankYouSummary  = 1,
PhoneCall =2,
MarketingLetter=3,
InPersonMeeting=4,
Email=5,           //, Sales Nav InMail
BigBoard= 6,
FedExMail=7,
LinkedInConnection=8,
Custom= 9,
ClientInterest =10,// (golf, wine, etc)
SpecificGroupMeeting= 11,
Training= 12,
UtilizeProspectingLetter= 13,
VideoConference=14,
WebinarInvite=15,
MailSpecificMarketingMaterials= 16,
Book= 17,  // How to Get an Appt with Anyone,
ThirdPartyArticle= 18,
SmallGift= 19,
HandWrittenNote=20,
LifeEvent=21,// (B-Day, Ann, Wedding, Birth of child, etc)
NewsletterDistributionList=22
 };
*/
 typedef NS_ENUM(NSInteger, TouchType)
 {
 ThankYouSummary  = 1,
 PhoneCall =2,
 InPersonMeeting=3,
 Email=4,           // Sales Nav InMail
 FedExMail=5,
 Custom= 6,
 ClientInterest =7,// (golf, wine, etc)
 MailSpecificMarketingMaterials= 8,
 Book= 9,  // How to Get an Appt with Anyone,
 ThirdPartyArticle= 10,
 SmallGift= 11,
 HandWrittenNote=12,
 LifeEvent=13,// (B-Day, Ann, Wedding, Birth of child, etc)
 NewsletterDistributionList=14,
BigBoard= 15,
MarketingLetter=16,
LinkedInConnection=17,
SpecificGroupMeeting= 18,
Training= 19,
UtilizeProspectingLetter= 20,
VideoConference=21,
WebinarInvite=22,
 };
 


@interface Touch : NSObject

@property (nonatomic,strong) NSNumber *uuid;
@property (nonatomic,assign) TouchType type;
@property (nonatomic,assign) NSInteger customType;
@property (nonatomic,strong) Client *client;
@property (nonatomic,strong) NSDate *date;
@property (nonatomic,strong) NSDate *completedDate;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *note;
@property (nonatomic,assign) BOOL completed;
@property (nonatomic,strong) NSDate *createdAt;
@property (nonatomic,strong) NSDate *updatedAt;
@property (nonatomic,strong) NSString *urlLink;
@property (nonatomic) NSInteger mTouchId;
@property (nonatomic,assign) TouchColor currentColor;
@property (nonatomic) NSInteger company_touchid;

-(void)populateWithDictionary:(NSDictionary *)dictionary;
+(instancetype)initWithDictionary:(NSDictionary *)dictionary;

+(instancetype)initWithUpdateDictionary:(NSDictionary *)dictionary;
-(void)populateWithUpdateDictionary:(NSDictionary *)dictionary;
@end

//typedef NS_ENUM(NSInteger, TouchType) {
//    PhoneCall = 21, //for a specific purpose
//    Email = 23, //for a specific purpose
//    InPersonMetting = 3,
//    FuzeMeeting = 4,
//    WebinarInvite = 5,
//    FedexMailing = 24,//mailing of marketing materials-specific
//    Book = 7, //Retirement Miracle, motivation, etc
//    ThirdPartyArticle = 8, //With post it note-hand written
//    SmallGift = 9,//
//    AffinityMeeting = 25,// invite-wine tasting, golf, etc
//    EducationalMeeting = 11, // invite (attorney, CPA, etc.)
//    HandWrittenThankYouNote = 20,
//    //EmailSummaryNote, // from previous meeting or phone conversation
//    BirthdayCardOrCall = 13,
//    NewsletterDistributionList = 14,
//    UtilizeProspectingLetters = 22,
//    Training = 16,
//    SpecificGroupMeetingFollowUp = 17,
//    ActivityThatInterestsClient = 18, // (golf, wine, hunting, etc.)
//    Custom = 19,
//    LinkedInConnection=26,
//    Anniversary=27
//};