//
//  MasterTouch.h
//  FollowUp Fox
//
//  Created by Priya on 04/02/16.
//  Copyright © 2016 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MasterTouch : NSObject

@property (nonatomic) NSInteger touch_id;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *note;
@property (nonatomic,strong) NSString *title;
@property (nonatomic) NSInteger number;
@property (nonatomic) NSInteger company_touchid;
-(instancetype)initWithDictionary:(NSDictionary *)dicionary;
-(void) save;
@end
