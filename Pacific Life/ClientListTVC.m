//
//  ClientListTVC.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 16/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "ClientListTVC.h"
#import "MBProgressHUD.h"
@interface ClientListTVC ()

@end

@implementation ClientListTVC
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _refreshControl = [[TableRefreshControl alloc]init];
    [self.theTableView addSubview:_refreshControl];
    [self.theTableView setBackgroundColor:[UIColor clearColor]];
    self.theTableView.tableFooterView  = [[UIView alloc]initWithFrame:CGRectZero];
    [_refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
   _touches = [NSMutableArray array];
    [self refreshTable];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTouches) name:@"refreshScreen" object:nil];

}
- (void)refreshTable
{
    
    //TODO: refresh your data
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [RequestManager updateProcessWithSuccessBlock:^{
        //
           [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [_refreshControl endRefreshing];
        _touches = [NSMutableArray array];
        [self getTouches];
        [self.theTableView reloadData];
    } failureBlock:^{
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [_refreshControl endRefreshing];
    }];
    
    //[self.mytableView reloadData];
}

- (IBAction)seeTouchesByClientAction:(id)sender
{
    UIButton *senderButton = (UIButton*)sender;
    
    /*
    NSLog(@"Superview: %@",senderButton.superview.superview.superview);
    _selectedIndexPath = [(UITableView *)senderButton.superview.superview.superview.superview.superview indexPathForCell:(UITableViewCell *)senderButton.superview.superview.superview];*/
    
    [self.theTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _selectedIndexPath = [NSIndexPath indexPathForItem:senderButton.tag inSection:0];
    [self performSegueWithIdentifier:@"touchesByClientSegue" sender:self];
}

-(void)viewWillAppear:(BOOL)animated
{
    _touches = [NSMutableArray array];
    [self getTouches];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"ClientUpdated"])
    {
        [self refreshTable];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"ClientUpdated"];
    }
    [super viewWillAppear:animated];
}

-(void)getTouches
{
    NSInteger badgeCount = 0;
    _touches 	= [NSMutableArray array];
    _touches = [DatabaseManager.instance allFutureTouches];
    for (Touch *touch in _touches)
    {
        NSInteger hours = [[[NSCalendar currentCalendar] components:NSHourCalendarUnit fromDate:[NSDate date]toDate:touch.date options:0] hour];
        if(hours <= 24)
        {
            badgeCount ++;
        }
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BadgeCount Test" message:[NSString stringWithFormat:@"BadgeCount : %ld",badgeCount] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
    [self.theTableView reloadData];
    [_clipBoardTableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int countOfGrays =[self countOfGrays];
    return _touches.count- countOfGrays;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ClientListCell *cell;
    Touch *touch = [_touches objectAtIndex:indexPath.row];
    if(touch.currentColor==Red)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"clientCellRed" forIndexPath:indexPath];
    }else if(touch.currentColor==Yellow)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"clientCellYellow" forIndexPath:indexPath];
    }else if(touch.currentColor==Green)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"clientCellGreen" forIndexPath:indexPath];
    }
    else if(touch.currentColor==Gray)
    {
       
    }

    
    [cell setupCellForTouch:touch andIndexPath:indexPath];
    //NSLog(@"Date: %@",touch.date);
    // Configure the cell...
    //cell.delegate = self;
    return cell;
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 55;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    Touch *touch;

    touch = _touches.count>0?[_touches objectAtIndex:0]:nil;

    if(touch.currentColor==Red)
    {
        view.backgroundColor = RedColor;
    }
    else if(touch.currentColor == Yellow)
    {
        view.backgroundColor = YellowColor;
    }
    else if(touch.currentColor == Green)
    {
        view.backgroundColor = GreenColor;
    }
    return view;
}

- (IBAction)unwindToClientList:(UIStoryboardSegue *)unwindSegue
{
  //  _touches = [NSMutableArray array];
//    [self getTouches];
//    [self.theTableView reloadData];

}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath * indexPath = _selectedIndexPath;
    if([segue.identifier isEqualToString:@"touchesByClientSegue"])
    {
        TouchListTVC *touchListVC = (TouchListTVC*)segue.destinationViewController;
        touchListVC.client = [[_touches objectAtIndex:indexPath.row] client];
    } else if([segue.identifier isEqualToString:@"seeClientDetailSegue"])
    {

        NewClientVC *newClientVC = (NewClientVC*)segue.destinationViewController;
        newClientVC.client = [[_touches objectAtIndex:indexPath.row] client];
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)seeClientDetailAction:(id)sender {
    UIButton *senderButton = (UIButton*)sender;
    /*
    NSLog(@"Superview: %@",senderButton.superview.superview.superview);
    _selectedIndexPath = [(UITableView *)senderButton.superview.superview.superview.superview.superview indexPathForCell:(UITableViewCell *)senderButton.superview.superview.superview];*/
    
    _selectedIndexPath = [NSIndexPath indexPathForItem:senderButton.tag inSection:0];
    
    [self performSegueWithIdentifier:@"seeClientDetailSegue" sender:self];
}
-(int)countOfGrays
{
    int counter=0;
    for(Touch *touch in _touches)
    {
        if(touch.currentColor==Gray)
        {
            counter++;
        }
    }
    return counter;
}
- (IBAction)logOutButtonTapped:(id)sender
{
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [RequestManager logoutWithCompletionBlock:^(BOOL success, NSError *error)
     {
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         if (success)
         {
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Logout Alert" message:@"Logout successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
             [DatabaseManager.instance deleteAllDataFromTables];
             [self updateBadgeCount];
           
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"loggedIn"];
             [self dismissViewControllerAnimated:YES completion:nil];

         }
         else
         {
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Logout Alert" message:@"Unable to Logout" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }
    }];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastFullSync"];
}
-(void) updateBadgeCount
{
    NSInteger badgeCount=0;
    NSArray  *touches = [[DatabaseManager instance] allFutureTouches];
    for (Touch *touch in touches)
    {
        NSInteger hours = [[[NSCalendar currentCalendar] components:NSHourCalendarUnit fromDate: [NSDate date]toDate:touch.date options:0] hour];
        if(hours <= 24)
        {
            badgeCount ++;
        }
    }
    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
}


@end
