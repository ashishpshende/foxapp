//
//  TouchListTVC.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchListCell.h"
#import "Client.h"
#import "DatabaseManager.h"
#import "Touch.h"
#import "TouchListCell.h"
#import "TouchDetailVC.h"
#import "ClientListTVC.h"
#import "RequestManager.h"
#import "TableRefreshControl.h"

@interface TouchListTVC : UIViewController <TouchListCellDelegate, SWTableViewCellDelegate>
@property (nonatomic,strong) NSMutableArray *touches;
@property (weak, nonatomic) IBOutlet UITableView *theTableView;
@property (strong,nonatomic) Client *client;
@property (strong,nonatomic) NSDateFormatter *formatter;
@property (nonatomic,strong) TableRefreshControl *refreshControl;
- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *clientName;
@property int grayCount;

- (IBAction)addTouchAction:(id)sender;
- (IBAction)onboardButtonAction:(id)sender;
- (IBAction)puntButtonAction:(id)sender;
- (IBAction)seeTouchDetails:(id)sender;

@property (nonatomic,strong) NSIndexPath *selectedIndexPath;
@end
