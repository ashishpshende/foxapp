    //
//  TouchListCell.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "TouchListCell.h"
#import "TouchListTVC.h"
@implementation TouchListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)handleLongPressGestures:(UILongPressGestureRecognizer *)sender
{
}
- (void)selectTextInTextField:(UITextField *)textField range:(NSRange)range {
    UITextPosition *from = [textField positionFromPosition:[textField beginningOfDocument] offset:range.location];
    UITextPosition *to = [textField positionFromPosition:from offset:range.length];
    [textField setSelectedTextRange:[textField textRangeFromPosition:from toPosition:to]];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if(buttonIndex==1)
//    {
//        UITextField *text = [alertView textFieldAtIndex:0];
//        if(text.text.length>0)
//        _touch.name = text.text;
////        [DatabaseManager.instance changeActivity:text.text toTouch:_touch];
////        
////        [self.tableDelegate touchChanged];
//        [RequestManager registerOrUpdateTouch:_touch withSuccessBlock:^(Touch *touch) {
//            //
//            [MBProgressHUD hideAllHUDsForView:self.superview animated:YES];
//            [self.tableDelegate reloadDataFromLocalDatabase];
//        } failureBlock:^{
//            //
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertView show];
//            [MBProgressHUD hideAllHUDsForView:self.superview animated:YES];
//        }];
//    
//    
//    }
}

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element
{
    self.selectedDate = selectedDate;
    _touch.date = selectedDate;
    MBProgressHUD *mbProgressHUD = [MBProgressHUD showHUDAddedTo:self.superview animated:YES];
    mbProgressHUD.labelText = @"Saving on the server...";
    [RequestManager registerOrUpdateTouch:_touch withSuccessBlock:^(Touch *touch) {
        //
        [MBProgressHUD hideAllHUDsForView:self.superview animated:YES];
        [self.tableDelegate touchChanged];
    } failureBlock:^{
        //
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [MBProgressHUD hideAllHUDsForView:self.superview animated:YES];
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupCellForTouch:(Touch*)touch andFormatter:(NSDateFormatter*)formatter
{
    self.touch = touch;
  //  NSLog(@"%ld : %@",self.touch.type,self.touch.name);
    self.iconLabel.text = [self touchNameForTouchType:touch.type];
    self.dateLabel.text = [formatter stringFromDate:touch.date];
    self.activityLabel.text = touch.name;
    
    if(touch.currentColor==Red)//(hours <= 24)
    {
        [self.iconImage setImage:[self imageForTouchType:touch.type isWhite:YES]];
        [self setLighterContent];
    }
    else if(touch.currentColor==Yellow)//((hours > 24)&&(hours<(24*7)))
    {
        [self setDarkerContent];
        [self.iconImage setImage:[self imageForTouchType:touch.type isWhite:NO]];
    }
    else if(touch.currentColor==Green)//if(hours>(24*7))
    {
        [self setDarkerContent];
        [self.iconImage setImage:[self imageForTouchType:touch.type isWhite:NO]];
    }else if(touch.currentColor==Gray)//if(hours>(24*7))
    {
        [self setDarkerContent];
        [self.iconImage setImage:[self imageForTouchType:touch.type isWhite:NO]];
    }
    
    if (touch.type == Custom  || touch.type > 22)
    {
        NSLog(@"NEW");
    }
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    if (!touch.completed)
    {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:34/255.0f green:131.0/255.0f blue:196.0/255.0f alpha:1.0f]
                                                    title:@"Complete"];
    }
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Delete"];
    
    
    self.rightUtilityButtons = rightUtilityButtons;

}

- (IBAction)showMoreTouchesAction:(id)sender
{
    
    __block   NSMutableArray *touchTypeNames = [NSMutableArray array];
    
    __block   NSArray *touches = [[DatabaseManager instance] getAllMasterTouches];
    for (MasterTouch *touch in touches)
    {
        [touchTypeNames addObject:touch.title];
    }
 
        [ActionSheetStringPicker showPickerWithTitle:@"Select a Touch"
                                            rows:touchTypeNames
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           
                                    
                                    //       NSArray *idsArray = [[DatabaseManager instance] getAllMasterids];
                                           //NSNumber *selectedNumber = [idsArray objectAtIndex:selectedIndex];

                                           MasterTouch *mTouch = (MasterTouch*) [touches objectAtIndex:selectedIndex];
                                           _touch.type  = mTouch.company_touchid;
                                        
                                           MBProgressHUD *mbProgressHUD = [MBProgressHUD showHUDAddedTo:self.superview animated:YES];
                                           mbProgressHUD.labelText = @"Saving on the server...";
                                           
                                           [RequestManager registerOrUpdateTouch:_touch withSuccessBlock:^(Touch *touch) {
                                               //
                                               [MBProgressHUD hideAllHUDsForView:self.superview animated:YES];
                                               [self.tableDelegate touchChanged];
                                           } failureBlock:^{
                                               //
                                               UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                               [alertView show];
                                               [MBProgressHUD hideAllHUDsForView:self.superview animated:YES];
                                           }];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:self];
    
}

- (IBAction)changeDateAction:(id)sender

  {
    /*
     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Gestures" message:@"Long Gesture Detected" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alertView show];
     */
    
    [ActionSheetDatePicker showPickerWithTitle:@"Choose a date" datePickerMode:UIDatePickerModeDate selectedDate:_touch.date target:self action:@selector(dateWasSelected:element:) origin:self];
    
}

-(void)setDarkerContent
{
    self.iconLabel.textColor = DarkGrayColor;
    //[self.accessoryImage setImage:[UIImage imageNamed:@"check.png"]];
}
-(void)setLighterContent
{
    self.iconLabel.textColor = WhiteColor;
   //[self.accessoryImage setImage:[UIImage imageNamed:@"check.png"]];
}
-(UIImage*)imageForTouchType:(TouchType)imageType isWhite:(BOOL)isWhite
{
    NSString *iconName;
    
    
    switch (imageType) {
        case PhoneCall:
            iconName = @"phone_call%@.png";
            break;
        case Email:
            iconName = @"email%@.png";
            break;
        case InPersonMeeting:
            iconName = @"in_person_meeting%@.png";
            break;
        case BigBoard:
            iconName = @"custom_post%@.png";
            break;
        case MailSpecificMarketingMaterials:
            iconName = @"email%@.png";
            break;
        case FedExMail:
            iconName = @"mail%@.png";
            break;
        case Book:
            iconName = @"book%@.png";
            break;
        case ThirdPartyArticle:
            iconName = @"mailing_marketing_materials%@.png";
            break;
        case SmallGift:
            iconName = @"small_gift%@.png";
            break;
        case WebinarInvite:
            iconName = @"webinar_invite_inverted%@.png";
            break;
        case ClientInterest:
            iconName = @"activity_client_interest%@.png";
            break;
        case HandWrittenNote:
            iconName = @"thankyou_note%@.png";
            break;
        case ThankYouSummary:
            iconName = @"thankyou_note%@.png";
            break;
        case LifeEvent:
            iconName = @"birthday%@.png";
            break;
        case NewsletterDistributionList:
            iconName = @"newsletter%@.png";
            break;
        case UtilizeProspectingLetter:
            iconName = @"prospecting_letters%@.png";
            break;
        case Training:
            iconName = @"training%@.png";
            break;
        case SpecificGroupMeeting:
            iconName = @"group_meeting_followup%@.png";
            break;
        case Custom:
            iconName = @"custom_post%@.png";
            break;
        case LinkedInConnection:
            iconName = @"linkedin%@.png";
            break;
        case MarketingLetter:
            iconName =@"mailing_marketing_materials%@.png";
            break;
        case VideoConference:
            iconName =@"webinar_invite%@.png";
             break;
        default:
            iconName = @"custom_post%@.png";
            break;
    }
    if(isWhite)
        iconName =[NSString stringWithFormat:iconName,@"_white"];
    else iconName =[NSString stringWithFormat:iconName,@""];
    UIImage *image = [UIImage imageNamed:iconName];
    return image;
}

-(NSString*)touchNameForTouchType:(TouchType)touchType
{
    NSArray *mtouches =[[DatabaseManager instance] getAllMasterTouches];
    for (MasterTouch *mTouch in mtouches)
    {
        if (mTouch.touch_id == touchType)
        {
            //NSLog(@"%ld : %@",(long)touchType,mTouch.title);
            return mTouch.title;
        }
        
    }

    return @"CUSTOM";
}

- (IBAction)changeTextAction:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] init];
    alertView.title = @"Replace the activity";
    alertView.message = @"Please type the new activity";
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView addButtonWithTitle:@"Cancel"];
    [alertView addButtonWithTitle:@"OK"];
    [alertView setDelegate:self];
    UITextField *text = [alertView textFieldAtIndex:0];
    if(![_touch.name isEqualToString:@"Intended Activity"])
    {
        text.text = _touch.name;
    }
    //[alertView show];
    
}
@end
