//
//  NewClientVC.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 27/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "NewClientVC.h"
#import "ClientListTVC.h"
@interface NewClientVC ()

@end

@implementation NewClientVC

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupStates];
    if(!_client)
    {
        _creatingNew = YES;
        _client = [Client new];
        [_nameLabel becomeFirstResponder];
    }
    else
    {
        [_cancelOrBackButton setTitle:@"Back" forState:UIControlStateNormal];
        [_saveButton setHidden:NO];
        [_saveButton setTitle:@"Update" forState:UIControlStateNormal];
        [_actionButton setTitle:@"VIEW TOUCH POINTS" forState:UIControlStateNormal];
        [_changeRatingButton setTitle:[_client.rating uppercaseString] forState:UIControlStateNormal];
        
        //_nameLabel.text = _client.name;
        [_nameLabel setHidden:YES];
        _nameTextField.text = _client.name;
        _companyTextField.text = _client.company;
        _emailTextField.text = _client.email;
        _phoneTextField.text = _client.phone;
        _addressTextField.text = _client.address;
        [_changeStateButton setTitle:[_statesAbbreviation objectAtIndex:[self findStateIndex]] forState:UIControlStateNormal];
        _cityTextField.text = _client.city;
        _zipTextField.text = _client.zip;
    }
}

-(void)setupStates
{
    _statesNames = [NSArray arrayWithObjects:@"Alabama",
                    @"Alaska",
                    @"Arizona",
                    @"Arkansas",
                    @"California",
                    @"Colorado",
                    @"Connecticut",
                    @"Delaware",
                    @"Florida",
                    @"Georgia",
                    @"Hawaii",
                    @"Idaho",
                    @"Illinois",
                    @"Indiana",
                    @"Iowa",
                    @"Kansas",
                    @"Kentucky",
                    @"Louisiana",
                    @"Maine",
                    @"Maryland",
                    @"Massachusetts",
                    @"Michigan",
                    @"Minnesota",
                    @"Mississippi",
                    @"Missouri",
                    @"Montana",
                    @"Nebraska",
                    @"Nevada",
                    @"New Hampshire",
                    @"New Jersey",
                    @"New Mexico",
                    @"New York",
                    @"North Carolina",
                    @"North Dakota",
                    @"Ohio",
                    @"Oklahoma",
                    @"Oregon",
                    @"Pennsylvania",
                    @"Rhode Island",
                    @"South Carolina",
                    @"South Dakota",
                    @"Tennessee",
                    @"Texas",
                    @"Utah",
                    @"Vermont",
                    @"Virginia",
                    @"Washington",
                    @"West Virginia",
                    @"Wisconsin",
                    @"Wyoming", nil];
    
    _statesAbbreviation = [NSArray arrayWithObjects:@"AL",
                           @"AK",
                           @"AZ",
                           @"AR",
                           @"CA",
                           @"CO",
                           @"CT",
                           @"DE",
                           @"FL",
                           @"GA",
                           @"HI",
                           @"ID",
                           @"IL",
                           @"IN",
                           @"IA",
                           @"KS",
                           @"KY",
                           @"LA",
                           @"ME",
                           @"MD",
                           @"MA",
                           @"MI",
                           @"MN",
                           @"MS",
                           @"MO",
                           @"MT",
                           @"NE",
                           @"NV",
                           @"NH",
                           @"NJ",
                           @"NM",
                           @"NY",
                           @"NC",
                           @"ND",
                           @"OH",
                           @"OK",
                           @"OR",
                           @"PA",
                           @"RI",
                           @"SC",
                           @"SD",
                           @"TN",
                           @"TX",
                           @"UT",
                           @"VT",
                           @"VA",
                           @"WA",
                           @"WV",
                           @"WI",
                           @"WY", nil];
}

-(int)findStateIndex
{
    int stateIndex = 0;
    if(_client.state)
        for(int i = 0; i<_statesNames.count; i++)
        {
            NSString *stateName = [_statesNames objectAtIndex:i];
            if([stateName isEqualToString:_client.state])
            {
                stateIndex = i;
            }
        }
    return  stateIndex;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}

- (IBAction)showStatesPicker:(id)sender {
   
    [ActionSheetStringPicker showPickerWithTitle:@"Select a Touch "
                                            rows:_statesNames
                                initialSelection:[self findStateIndex]
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           [_changeStateButton setTitle:[_statesAbbreviation objectAtIndex:selectedIndex] forState:UIControlStateNormal];
                                           _client.state =[_statesNames objectAtIndex:selectedIndex];
                                           [_zipTextField becomeFirstResponder];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:_changeStateButton];
}

- (IBAction)showRatingPicker:(id)sender
{
    [ActionSheetStringPicker showPickerWithTitle:@"Select a Rating "
                                            rows:[NSArray arrayWithObjects:@"A",@"B",@"C", nil]
                                initialSelection:[[_client.rating uppercaseString]isEqualToString:@"A"]?0:[[_client.rating uppercaseString]isEqualToString:@"B"]?1:[[_client.rating uppercaseString]isEqualToString:@"C"]?3:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           [_changeRatingButton setTitle: selectedValue
                                                               forState:UIControlStateNormal];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:_changeStateButton];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _nameTextField)
    {
        [_companyTextField becomeFirstResponder];
    }
    else if (textField == _companyTextField)
    {
        [_emailTextField becomeFirstResponder];
    }
    else if (textField == _emailTextField)
    {
        [_phoneTextField becomeFirstResponder];
    }
    else if (textField == _phoneTextField)
    {
        [_addressTextField becomeFirstResponder];
    }
    else if (textField == _addressTextField)
    {
        [_cityTextField becomeFirstResponder];
    }
    else if (textField == _cityTextField)
    {
        [_cityTextField resignFirstResponder];
        [self showStatesPicker:self];
    }
    else if (textField == _zipTextField)
    {
        if(_creatingNew)
        {
            [self createClientAction:self];
        }
        else
        {
            [self saveAction:self];
        }
        
    }
    return YES;
}


- (IBAction)createClientAction:(id)sender {
    if(_creatingNew)
    {
        if([_nameTextField.text length]>0)
        {
            [self saveAction:self];
        }
        else
        {
            [_nameTextField becomeFirstResponder];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Name field required" message:@"Please fill the name field" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }
    else
    {
        [self performSegueWithIdentifier:@"touchesByClientSegue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //UIButton *senderButton = (UIButton*)sender;
    //NSLog(@" A = %@",[senderButton superview]);
    if([segue.identifier isEqualToString:@"touchesByClientSegue"])
    {
        TouchListTVC *touchListVC = (TouchListTVC*)segue.destinationViewController;
        touchListVC.client = _client;
    }
    else if ([[segue identifier] isEqualToString:@"backToClientListSegue"])
    {
        ClientListTVC *clientListTVC = (ClientListTVC*)segue.destinationViewController;
        [clientListTVC.theTableView reloadData];
    }
}
- (IBAction)saveAction:(id)sender {
    _mbProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _mbProgressHUD.labelText = @"Saving on the server...";
    _client.rating = _changeRatingButton.titleLabel.text;
    _client.name = _nameTextField.text;
    _client.company = _companyTextField.text?_companyTextField.text:@"";
    _client.email = _emailTextField.text?_emailTextField.text:@"";
    _client.phone = _phoneTextField.text?_phoneTextField.text:@"";
    _client.address = _addressTextField.text?_addressTextField.text:@"";;
    _client.city = _cityTextField.text?_cityTextField.text:@"";
    _client.zip = _zipTextField.text?_zipTextField.text:@"";
    _client.state = _client.state?_client.state:@"";
    _client.status = _client.status?_client.status:@"A";
    [RequestManager registerOrUpdateClient:_client withSuccessBlock:^(Client *client) {
        //
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ClientUpdated"];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sucess" message:_creatingNew?@"Client sucessfully created":@"Client sucessfully updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertView.tag =1;
        [alertView show];
     
    } failureBlock:^{
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
    [self performSegueWithIdentifier:@"backToClientListSegue" sender:self];
    }
}



@end
