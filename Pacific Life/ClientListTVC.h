//
//  ClientListTVC.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 16/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "DatabaseManager.h"
#import "RequestManager.h"
#import "ClientListCell.h"
#import "TouchListTVC.h"
#import "TableRefreshControl.h"
#import "NewClientVC.h"
#import "AppConstants.h"
@interface ClientListTVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *clipBoardTableView;
@property (weak, nonatomic) IBOutlet UIView *clipboardView;
@property (strong,nonatomic) IBOutlet UITableView *theTableView;
@property (nonatomic,strong) NSArray *touches;
@property (nonatomic,strong) TableRefreshControl *refreshControl;
- (IBAction)seeClientDetailAction:(id)sender;
//- (IBAction)showClientInfo:(id)sender;
- (void)refreshTable;
- (IBAction)seeTouchesByClientAction:(id)sender;
@property (nonatomic,strong) NSIndexPath *selectedIndexPath;

@end
