//
//  TouchDetailVC.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 1/06/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Client.h"
#import "Touch.h"
#import "ActionSheetPicker.h"
#import "DatabaseManager.h"
#import "RequestManager.h"
#import "MBProgressHUD.h"
#import "TouchListTVC.h"
@interface TouchDetailVC : UIViewController <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *clientName;
@property (strong,nonatomic) Client *client;
@property (strong,nonatomic) Touch *touch;
@property (nonatomic,assign) int touchNumber;

@property (weak, nonatomic) IBOutlet UIView *touchView;
@property (weak, nonatomic) IBOutlet UILabel *touchNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *iconLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityLabel;
@property (weak, nonatomic) IBOutlet UILabel *completedDateLabel;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (weak, nonatomic) IBOutlet UIButton *completeDate;
- (IBAction)completeDateAction:(id)sender;
@property (nonatomic, strong) NSDate *selectedDate;
- (IBAction)saveAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *referenceLinkButton;
- (IBAction)RefernceLinkButtonTapped:(id)sender;


@end
