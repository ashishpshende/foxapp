//
//  ClientDetailVC.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 30/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Client.h"
#import "TouchListTVC.h"
@interface ClientDetailVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backAction;

@property (nonatomic,strong) Client *client;


@property (weak, nonatomic) IBOutlet UILabel *nameLargeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *zipLabel;

@end
