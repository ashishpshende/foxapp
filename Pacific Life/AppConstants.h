//
//  AppConstants.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 2/06/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>
#define RedColor [UIColor colorWithRed:158/255.0 green:56/255.0 blue:46/255.0 alpha:1.0];
#define GreenColor [UIColor colorWithRed:165/255.0 green:201/255.0 blue:131/255.0 alpha:1.0];
#define YellowColor [UIColor colorWithRed:222/255.0 green:186/255.0 blue:79/255.0 alpha:1.0];
#define GrayColor [UIColor colorWithRed:165/255.0 green:165/255.0 blue:165/255.0 alpha:1.0];
#define WhiteColor [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
#define DarkGrayColor [UIColor colorWithRed:55/255.0 green:55/255.0 blue:55/255.0 alpha:1.0];
@interface AppConstants : NSObject

@end
