//
//  TouchDetailVC.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 1/06/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "TouchDetailVC.h"
#import "AppConstants.h"
@interface TouchDetailVC ()

@end

@implementation TouchDetailVC
@synthesize referenceLinkButton;
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _clientName.text = _client.name;
    if(_touch.completed)
    {
        [_completeDate setEnabled:NO];
        [_noteTextView setEditable:NO];
    }
    else
    {
        [_completeDate setEnabled:YES];
        [_noteTextView setEditable:YES];
    }
    _touchNumberLabel.text = [NSString stringWithFormat:@"%i",_touchNumber];
    self.iconLabel.text = [self touchNameForTouchType:_touch.type];
    //  [self.theTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if(_touch.completed)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterShortStyle];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
        
        if(_touch.currentColor == Red)
            self.completedDateLabel.textColor = [UIColor whiteColor];
        self.completedDateLabel.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:_touch.date]];
    }
    else
    {
        self.completedDateLabel.text = @"Select";
        
    }
    
    self.activityLabel.text = _touch.name;
    self.noteTextView.text = _touch.note;
    if(_touch.currentColor==Red)//(hours <= 24)
    {
        _touchView.backgroundColor = RedColor;
        [self.iconImage setImage:[self imageForTouchType:_touch.type isWhite:YES]];
        [self setLighterContent];
    }
    else if(_touch.currentColor==Yellow)//((hours > 24)&&(hours<(24*7)))
    {
        _touchView.backgroundColor = YellowColor;
        [self setDarkerContent];
        [self.iconImage setImage:[self imageForTouchType:_touch.type isWhite:NO]];
    }
    else if(_touch.currentColor==Green)//if(hours>(24*7))
    {
        _touchView.backgroundColor = GreenColor;
        [self setDarkerContent];
        [self.iconImage setImage:[self imageForTouchType:_touch.type isWhite:NO]];
    }else if(_touch.currentColor==Gray)//if(hours>(24*7))
    {
        _touchView.backgroundColor = GrayColor;
        [self setDarkerContent];
        [self.iconImage setImage:[self imageForTouchType:_touch.type isWhite:NO]];
    }
    // Do any additional setup after loading the view.
    
     _touch.urlLink = @"https://followupfox.com";
    [referenceLinkButton.titleLabel setTextAlignment:NSTextAlignmentLeft];
    referenceLinkButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    referenceLinkButton.titleLabel.numberOfLines = 0;
    [referenceLinkButton setTitle:_touch.urlLink forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setDarkerContent
{
    self.touchNumberLabel.textColor = DarkGrayColor;
    self.iconLabel.textColor = DarkGrayColor;
        self.activityLabel.textColor = DarkGrayColor;
    
}
-(void)setLighterContent
{
    self.touchNumberLabel.textColor = WhiteColor;
    self.activityLabel.textColor = WhiteColor;
    self.iconLabel.textColor = WhiteColor;
    
}
-(UIImage*)imageForTouchType:(TouchType)imageType isWhite:(BOOL)isWhite
{
    NSString *iconName;
    switch (imageType) {
        case PhoneCall:
            iconName = @"phone_call%@.png";
            break;
        case Email:
            iconName = @"email%@.png";
            break;
        case InPersonMeeting:
            iconName = @"in_person_meeting%@.png";
            break;
        case BigBoard:
            iconName = @"custom_post%@.png";
            break;
        case MailSpecificMarketingMaterials:
            iconName = @"email%@.png";
            break;
        case FedExMail:
            iconName = @"mail%@.png";
            break;
        case Book:
            iconName = @"book%@.png";
            break;
        case ThirdPartyArticle:
            iconName = @"mailing_marketing_materials%@.png";
            break;
        case SmallGift:
            iconName = @"small_gift%@.png";
            break;
        case WebinarInvite:
            iconName = @"webinar_invite_inverted%@.png";
            break;
        case ClientInterest:
            iconName = @"activity_client_interest%@.png";
            break;
        case HandWrittenNote:
            iconName = @"thankyou_note%@.png";
            break;
        case ThankYouSummary:
            iconName = @"thankyou_note%@.png";
            break;
        case LifeEvent:
            iconName = @"birthday%@.png";
            break;
        case NewsletterDistributionList:
            iconName = @"newsletter%@.png";
            break;
        case UtilizeProspectingLetter:
            iconName = @"prospecting_letters%@.png";
            break;
        case Training:
            iconName = @"training%@.png";
            break;
        case SpecificGroupMeeting:
            iconName = @"group_meeting_followup%@.png";
            break;
        case Custom:
            iconName = @"custom_post%@.png";
            break;
        case LinkedInConnection:
            iconName = @"linkedin%@.png";
            break;
        case MarketingLetter:
            iconName =@"mailing_marketing_materials%@.png";
            break;
        case VideoConference:
            iconName =@"webinar_invite%@.png";
        default:
            iconName = @"custom_post%@.png";
            break;
    }
    if(isWhite)
        iconName =[NSString stringWithFormat:iconName,@"_white"];
    else
        iconName =[NSString stringWithFormat:iconName,@""];
   
    UIImage *image = [UIImage imageNamed:iconName];
    return image;
}

-(NSString*)touchNameForTouchType:(TouchType)touchType
{

    for (MasterTouch *mTouch in [[DatabaseManager instance] getAllMasterTouches])
    {
        if (mTouch.touch_id == touchType)
        {
            return mTouch.title;
        }
    }
    
//    NSString *touchName;
//    switch (touchType) {
//        case PhoneCall:
//            touchName = @"PHONE CALL";
//            break;
//        case Email:
//            touchName = @"EMAIL";
//            break;
//        case InPersonMeeting:
//            touchName = @"IN PERSON MEETING";
//            break;
//        case VideoConference:
//            touchName = @"VIDEO CONFERENCE";
//            break;
//        case WebinarInvite:
//            touchName = @"WEBINAR INVITE";
//            break;
//        case FedExMail:
//            touchName = @"FED EX/MAIL";
//            break;
//        case Book:
//            touchName = @"BOOK";
//            break;
//        case ThirdPartyArticle:
//            touchName = @"THIRD PARTY ARTICLE";
//            break;
//        case SmallGift:
//            touchName = @"GIFT";
//            break;
//        case BigBoard:
//            touchName = @"BIG BOARD";
//            break;
//        case MailSpecificMarketingMaterials:
//            touchName = @"MAIL SPECIFIC MARKETING LETTER";
//            break;
//        case HandWrittenNote:
//            touchName = @"HAND WRITTEN NOTE";
//            break;
//        case LifeEvent:
//            touchName = @"LIFE EVENT";
//            break;
//        case NewsletterDistributionList:
//            touchName = @"NEWSLETTER";
//            break;
//        case UtilizeProspectingLetter:
//            touchName = @"UTILIZE PROSPECTING  LETTER";
//            break;
//        case Training:
//            touchName = @"TRAINING";
//            break;
//        case SpecificGroupMeeting:
//            touchName = @"GROUP MEETING";
//            break;
//        case ClientInterest:
//            touchName = @"ACTIVITY INTEREST";
//            break;
//        case Custom:
//            touchName = @"CUSTOM";
//            break;
//        case LinkedInConnection:
//            touchName = @"LINKEDIN CONNECTION";
//            break;
//        case ThankYouSummary:
//            touchName = @"THANK YOU";
//            break;
//        case MarketingLetter:
//            touchName = @"MARKETING LETTER";
//            break;
//        default:
//            touchName = @"CUSTOM";
//            break;
//    }
//    
//    return touchName;
    
    return  @"CUSTOM";
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}
- (IBAction)completeDateAction:(id)sender
{

    
    if(!_touch.completed)
    {
        //ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] target:self action:@selector(dateWasSelected:element:) origin:_completeDate];
        [ActionSheetDatePicker showPickerWithTitle:@"Choose a date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] target:self action:@selector(dateWasSelected:element:) origin:_completeDate];
    }
    
}

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element
{
    MBProgressHUD *mbProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    mbProgressHUD.labelText = @"Saving on the server...";
    
    self.selectedDate = selectedDate;
    _touch.completed = YES;
    _touch.completedDate=self.selectedDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    _touch.date = selectedDate;
    [DatabaseManager.instance changeCompletedDate:_touch.completedDate toTouch:_touch];
    [RequestManager registerOrUpdateTouch:_touch withSuccessBlock:^(Touch *touch) {
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self performSegueWithIdentifier:@"unwindToTouchListSegue" sender:self];
    } failureBlock:^{
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
    
    self.completedDateLabel.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:_selectedDate]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"unwindToTouchListSegue"])
    {
        TouchListTVC *touchListTVC = (TouchListTVC*)segue.destinationViewController;
        [touchListTVC.theTableView reloadData];
        
    }
}
- (IBAction)saveAction:(id)sender {
    _touch.note = _noteTextView.text;
    MBProgressHUD *mbProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    mbProgressHUD.labelText = @"Saving on the server...";
    [RequestManager registerOrUpdateTouch:_touch withSuccessBlock:^(Touch *touch) {
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        //UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Changes saved" message:@"Changes saved succesfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //[alertView show];
        [self performSegueWithIdentifier:@"unwindToTouchListSegue" sender:self];
        
    } failureBlock:^{
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

- (IBAction)RefernceLinkButtonTapped:(id)sender
{
    UIApplication *app = [UIApplication sharedApplication];
    NSURL *url = [NSURL URLWithString:_touch.urlLink];
    [app openURL:url];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
@end
