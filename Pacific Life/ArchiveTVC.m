//
//  ArchiveTVC.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 1/06/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "ArchiveTVC.h"

@interface ArchiveTVC ()

@end

@implementation ArchiveTVC
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getClientsInfo];
    UISwipeGestureRecognizer *oneFingerSwipeRight = [[UISwipeGestureRecognizer alloc]
                                                     initWithTarget:self
                                                     action:@selector(oneFingerSwipeRight:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer:oneFingerSwipeRight];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    [self.theTableView setBackgroundColor:[UIColor clearColor]];
    self.theTableView.tableFooterView  = [[UIView alloc]initWithFrame:CGRectZero];
      [self.theTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)getClientsInfo
{
    _clients = [DatabaseManager.instance allArchiveClients];
}
- (void)oneFingerSwipeRight:(UITapGestureRecognizer *)recognizer {
    // Insert your own code to handle swipe left
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_clients count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ArchiveClientCell *cell;

    Client *client = [_clients objectAtIndex:indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:@"archiveCell" forIndexPath:indexPath];
    [cell setupCellForClient:client andIndexPath:indexPath];
    cell.delegate = self;
    return cell;
}

#pragma mark - SWTableViewCell options menu methods
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *cellIndexPath = [self.theTableView indexPathForCell:cell];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    Client *clientToUnarchive = [_clients objectAtIndex:cellIndexPath.row];
    clientToUnarchive.status = @"A";
    [RequestManager registerOrUpdateClient:clientToUnarchive withSuccessBlock:^(Client *client)
    {
        //
        [RequestManager syncTouchesForClient:client withSuccessBlock:^(NSArray *touches) {
            //
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self getClientsInfo];
            [self.theTableView reloadData];
        } failureBlock:^{
            //
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }];
    } failureBlock:^{
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];

    }];
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 55;
}

@end
