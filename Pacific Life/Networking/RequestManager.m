//
//  RequestManager.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "RequestManager.h"

//static NSString * const BaseURLString = @"http://kogimobiletest.info/api";
//static NSString * const BaseURLString = @"http://ec2-174-129-137-169.compute-1.amazonaws.com/api";
static NSString* const BaseURLString = @"https://followupfox.com/api";

//static NSString * const BaseURLString = @"http://10.0.0.2:8000/api";
static BOOL PRODMODE = YES;

@implementation RequestManager
+(void) loginForUser:(NSString*)username password:(NSString*)password andCompletionBlock:(void(^)(BOOL success, NSString* errorDescription, NSError *error))loginCompletionBlock
{
    
    //Create Url and parameters
    NSString * url = [NSString stringWithFormat:@"%@/login",BaseURLString];
   
    NSString *apns =[[NSUserDefaults standardUserDefaults]  objectForKey:@"apns"];
   
    if (!apns) apns =@"";
    NSDictionary *parameters = [NSDictionary dictionaryWithObjects:@[username,password,apns] forKeys:@[@"username",@"password",@"apns"]];
    
    //Perform the request
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if (PRODMODE)
    {
    manager.securityPolicy.allowInvalidCertificates = YES;
    }
    manager.responseSerializer = [AFJSONResponseSerializer new]; // use an Json Serializer.
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // parse response
        
        NSDictionary* dictionaryResponse = (NSDictionary *)responseObject;
        if([[dictionaryResponse objectForKey:@"responseCode"] intValue]==100)
        {
            NSDictionary* dictionaryData = [dictionaryResponse objectForKey:@"data"];
            
            NSDictionary *userDict = [dictionaryData objectForKey:@"user"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[userDict objectForKey:@"seller_id"] forKey:@"seller_id"];
            [[NSUserDefaults standardUserDefaults] setObject:[userDict objectForKey:@"name"] forKey:@"name"];
            [[NSUserDefaults standardUserDefaults] setObject:[userDict objectForKey:@"username"] forKey:@"username"];
            [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
            [[NSUserDefaults standardUserDefaults] setObject:[userDict objectForKey:@"email"] forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] synchronize];
         //   NSArray* cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[operation.response allHeaderFields] forURL:operation.request.URL];
            
            
            if(![[NSUserDefaults standardUserDefaults] objectForKey:@"lastFullSync"])
            {
                [self fullDownloadProcessWithSuccessBlock:^{
                    //
                    loginCompletionBlock(YES, nil, nil);
                } failureBlock:^{
                    //
                    loginCompletionBlock(NO, @"Full download error", nil);
                }];
                
                NSArray *mTouches = [dictionaryData objectForKey:@"touchm"];
                for (NSDictionary *dictionary in mTouches)
                {
                    MasterTouch *mTouch = [[MasterTouch alloc]initWithDictionary:dictionary];
                    [mTouch save];
                }
                
            }
            else
            {
                [self updateProcessWithSuccessBlock:^{
                    //
                    loginCompletionBlock(YES, nil, nil);
                } failureBlock:^{
                    //
                    loginCompletionBlock(NO, @"Update process error", nil);
                }];
                
            }
        }
        else
        {
            loginCompletionBlock(NO, [dictionaryResponse objectForKey:@"reponseCode"], nil);
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // inform error.
              loginCompletionBlock(NO, @"Login error", error);
          }];

}

+(void) logoutWithCompletionBlock:(void(^)(BOOL success, NSError *error))logoutCompletionBlock
{
    // 1
    NSString *string = [NSString stringWithFormat:@"%@/logout", BaseURLString];
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // 2
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    if (PRODMODE)
    {
        operation.securityPolicy.allowInvalidCertificates = YES;
    }
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionaryResponse = (NSDictionary*)responseObject;
        if([[dictionaryResponse objectForKey:@"responseCode"] intValue]==100)
        {
            logoutCompletionBlock(YES,nil);
        }
        else
        {
            logoutCompletionBlock(NO,nil);
        }
        // 3
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         logoutCompletionBlock(NO,error);
         // 4
     }];
    // 5
    [operation start];
}


+(void)allActiveClientsSuccessBLock:(void(^)(NSArray* clients))successBlock failureBlock:(void(^)(void))failureBlock
{
    // 1
    NSString *string = [NSString stringWithFormat:@"%@/clients/%@", BaseURLString,[[NSUserDefaults standardUserDefaults] objectForKey:@"seller_id"]];
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // 2
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    if (PRODMODE)
    {
        operation.securityPolicy.allowInvalidCertificates = YES;
    }
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionaryResponse = (NSDictionary*)responseObject;
        NSMutableArray* clientsArray = [NSMutableArray new];
        if([[dictionaryResponse objectForKey:@"responseCode"] intValue]==100)
        {
            NSArray* arrayResponse = [dictionaryResponse objectForKey:@"data"];
            for(NSDictionary *clientDictionary in arrayResponse)
            {
                Client *client =   [Client initWithDictionary:clientDictionary];
                [clientsArray addObject:client];
                
            }
            successBlock(clientsArray);
        }
        else
        {
            failureBlock();
        }
        // 3
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        NSLog(@"Error: %@",error);
        failureBlock();
        // 4
            }];
    // 5
    [operation start];
}


+(void)allTouchesForClient:(Client*)client andSuccessBlock:(void(^)(NSArray* touches))successBlock failureBlock:(void(^)(void))failureBlock
{
    // 1
    NSString *string = [NSString stringWithFormat:@"%@/activities/%@", BaseURLString,client.uuid];
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // 2
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    if (PRODMODE)
    {
        operation.securityPolicy.allowInvalidCertificates = YES;
    }
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *touchesArray = [NSMutableArray new];
        NSDictionary *dictionaryResponse = (NSDictionary*)responseObject;
        if([[dictionaryResponse objectForKey:@"responseCode"] intValue]==100)
        {
            NSArray* arrayResponse = [dictionaryResponse objectForKey:@"data"];
            for(NSDictionary *touchDictionary in arrayResponse)
            {
                Touch *touch =   [Touch initWithDictionary:touchDictionary];
                [touchesArray addObject:touch];
            }
            
            for (Touch *touch in touchesArray)
            {
                [[DatabaseManager instance] insertTouch:touch];
            }
            
            successBlock(touchesArray);
        }
        else
        {
            failureBlock();
        }
        // 3
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         failureBlock();
         NSLog(@"Error: %@",error);
         // 4
     }];
    // 5
    [operation start];
}

+(void)fullDownloadProcessWithSuccessBlock:(void(^)(void))successBlock failureBlock:(void(^)(void))failureBlock
{
    [self allActiveClientsSuccessBLock:^(NSArray *clients) {
        //
        if(clients.count!=0)
        for(int i = 1; i<=clients.count; i++)
        {
            Client *client = [clients objectAtIndex:i-1];
            [DatabaseManager.instance insertClient:client];
            [self allTouchesForClient:client andSuccessBlock:^(NSArray *touches) {
                //
                if(touches.count!=0)
                for(int j = 1; j<=touches.count; j++)
                {
                    Touch *touch = [touches objectAtIndex:j-1];
                    [DatabaseManager.instance insertTouch:touch];
                    if((i == clients.count)&&(j == touches.count))
                    {
//                        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastFullSync"];
//                        [[NSUserDefaults standardUserDefaults] synchronize];
                        successBlock();
                    }
                    
                   
                }
                else
                    successBlock();
                
                 [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshScreen" object:@{}];
            } failureBlock:^{
                //
                failureBlock();
            }];
        }
        else
            successBlock();
    } failureBlock:^{
        failureBlock();
        //
    }];
}

+(void)updateProcessWithSuccessBlock:(void(^)(void))successBlock failureBlock:(void(^)(void))failureBlock
{
    BOOL loggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"loggedIn"];
    
    if (loggedIn)
    {
        
        [self fullDownloadProcessWithSuccessBlock:^{
            //
            successBlock();
        } failureBlock:^{
            //
            failureBlock();
        }];        
    }
    /*
    
    [self allActiveClientsSuccessBLock:^(NSArray *clients){
        if(clients.count!=0)
        {
            NSArray *clientsFromDB = [DatabaseManager.instance allClients];
            NSMutableSet *intersectionSet = [NSMutableSet new];
            for(Client *client in clientsFromDB)
            {
                [intersectionSet addObject:client];
            }
            
            NSSet *clientsSet = [NSSet setWithArray:clients];
            [intersectionSet intersectSet: clientsSet];
            NSArray *intersectionArray = [intersectionSet allObjects];
            NSMutableArray *toDeleteClients = [NSMutableArray arrayWithArray:intersectionArray];
            [toDeleteClients removeObjectsInArray:intersectionArray];
            for(Client *client in toDeleteClients)
            {
                [DatabaseManager.instance deleteClient:client andDeleteRelatedTouches:YES];
            }
            if([[DatabaseManager.instance allClients] count]>0)
            for(Client *dbClient in [DatabaseManager.instance allClients])
            {
                for(int i = 1 ; i <=clients.count; i++)
                {
                    Client *client = [clients objectAtIndex:i-1];
                    if([client.uuid intValue]==[dbClient.uuid intValue])
                    {
                        if ([dbClient.updatedAt compare:client.updatedAt] == NSOrderedAscending)
                        {
                            [DatabaseManager.instance deleteClient:dbClient andDeleteRelatedTouches:NO];
                            [DatabaseManager.instance insertClient:client];
                        }
                    }
                }
            }
            else
            {
                for(int i = 1 ; i <=clients.count; i++)
                {
                    Client *client = [clients objectAtIndex:i-1];
                    [DatabaseManager.instance insertClient:client];
                }
            }
        }
        else
        {
            for(Client *dbClient in [DatabaseManager.instance allClients])
            {
                [DatabaseManager.instance deleteClient:dbClient andDeleteRelatedTouches:YES];
            }
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastFullSync"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            successBlock();

        }
    } failureBlock:^{
        failureBlock();
    }];
    
    NSArray *clients = [DatabaseManager.instance allClients];
    for(int i=1; i<=clients.count; i++)
    {
        Client *client = [clients objectAtIndex:i-1];
        [self allTouchesForClient:client andSuccessBlock:^(NSArray *touches) {
            if(touches.count!=0)
            {
                NSArray *touchesFromDB = [DatabaseManager.instance allTouches];
                NSMutableSet *intersectionSet = [NSMutableSet new];
                for(Touch *touch in touchesFromDB)
                {
                    [intersectionSet addObject:touch];
                }
                [intersectionSet intersectSet:[NSSet setWithArray:touches]];
                NSArray *intersectionArray = [intersectionSet allObjects];
                NSMutableArray *toDeleteTouches = [NSMutableArray arrayWithArray:intersectionArray];
                [toDeleteTouches removeObjectsInArray:intersectionArray];
                for(Touch *touch in toDeleteTouches)
                {
                    [DatabaseManager.instance deleteTouch:touch];
                }
                
                for(int j = 1; j<=touches.count; j++)
                {
                    Touch *touch = [touches objectAtIndex:j-1];
                    NSArray *touchesForClientFromDB = [DatabaseManager.instance touchesForClient:client];
                    if(touchesForClientFromDB.count>0)
                    for(Touch *dbTouch in touchesForClientFromDB)
                    {
                        if([touch.uuid intValue]==[dbTouch.uuid intValue])
                        {
                            if ([dbTouch.updatedAt compare:touch.updatedAt] == NSOrderedAscending)
                            {
                                NSString *touchName = dbTouch.name;
                                [DatabaseManager.instance deleteTouch:touch];
                                
                                // Add the touch name (from the DB) to the updated touch
                                [DatabaseManager.instance deleteTouch:dbTouch];
                                touch.name = touchName;
                                [DatabaseManager.instance insertTouch:touch];
                            }
                        }
                    }
                    else
                    {
                        [DatabaseManager.instance insertTouch:touch];
                    }
                    NSLog(@"i = %i - j = %i",i,j);
                    if((i == clients.count)&&(j == touches.count))
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastFullSync"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        successBlock();
                    }
                }
            }
            else
            {
                if(i == clients.count)
                {
                    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastFullSync"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    successBlock();
                }
            }
            
        } failureBlock:^{
            //
            failureBlock();
        }];
        //i++;
    }
 */
}

#pragma mark - Post Data

+(void) registerOrUpdateClient:(Client*)client withSuccessBlock:(void(^)(Client* client))successBlock failureBlock:(void(^)(void))failureBlock
{
    //Create Url and parameters
    NSString * url = [NSString stringWithFormat:@"%@/register-update-clients/%@",BaseURLString,[[NSUserDefaults standardUserDefaults] objectForKey:@"seller_id"]];
    
    NSDictionary *parameters;
    if(client.uuid)
    {
        parameters = [NSDictionary dictionaryWithObjects:@[client.uuid, client.status?client.status:@"", client.name,client.company?client.company:@"",client.email?client.email:@"",client.phone?client.phone:@"",client.address?client.address:@"",client.state?client.state:@"",client.city?client.city:@"",client.zip?client.zip:@"",client.rating?client.rating:@""] forKeys:@[@"client_id",@"status", @"name",@"company",@"email",@"phone",@"address",@"state",@"city",@"zip",@"rating"]];
    }
    else
    {
        parameters = [NSDictionary dictionaryWithObjects:@[client.status, client.name,client.company,client.email,client.phone,client.address,client.state,client.city,client.zip,client.rating] forKeys:@[@"status",@"name",@"company",@"email",@"phone",@"address",@"state",@"city",@"zip",@"rating"]];
    }
    
    //Perform the request
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer new]; // use an Json Serializer.
    if (PRODMODE)
    {
        manager.securityPolicy.allowInvalidCertificates = YES;
    }
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // parse response
        
        NSDictionary* dictionaryResponse = (NSDictionary *)responseObject;
        if([[dictionaryResponse objectForKey:@"responseCode"] intValue]==200)
        {
            failureBlock();
        }
        else
        {
            NSDictionary* dictionaryData = [dictionaryResponse objectForKey:@"data"];
            Client *client =   [Client initWithDictionary:dictionaryData];
            
            if ([client.status isEqualToString:@"CW"] || [client.status isEqualToString:@"CL"])
            {
            client.updatedAt =   [NSDate date];
            }
            
            if([[DatabaseManager.instance clientWithUuid:client.uuid] uuid]) //Client already existed on the DB
            {
                [DatabaseManager.instance deleteClient:client andDeleteRelatedTouches:YES];
                [DatabaseManager.instance insertClient:client];
                successBlock(client);
            }
            else // Client is new in the DB
            {
                
                [DatabaseManager.instance insertClient:client];
                [self allTouchesForClient:client andSuccessBlock:^(NSArray *touches) {
                    //
                    for(Touch *touch in touches)
                    {
                        [DatabaseManager.instance insertTouch:touch];
                    }
                    successBlock(client);
                } failureBlock:^{
                    //
                    failureBlock();
                }];
            }
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // inform error.
              failureBlock();
          }];
}
+(void) syncTouchesForClient:(Client*)client withSuccessBlock:(void(^)(NSArray* touches))successBlock failureBlock:(void(^)(void))failureBlock
{
    [self allTouchesForClient:client andSuccessBlock:^(NSArray *touches) {
        //
        for(Touch *touch in touches)
        {
            
            if([[[DatabaseManager instance] touchWithUuid:touch.uuid] uuid])
            {
                [[DatabaseManager instance] deleteTouch:touch];
                [[DatabaseManager instance] insertTouch:touch];
            }
        }
        successBlock(touches);
    } failureBlock:^{
        //
        failureBlock();
    }];
    
    
}

+(void) completeTouch:(Touch*)touch withSuccessBlock:(void(^)(Touch* touch))successBlock failureBlock:(void(^)(void))failureBlock
{
    //Create Url and parameters
    NSString * url = [NSString stringWithFormat:@"%@/activities",BaseURLString];
    NSDictionary *parameters;
    touch.completed = YES;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    touch.date = [NSDate date];
    if(touch.uuid)
    {
        parameters = [NSDictionary dictionaryWithObjects:@[
                                                           touch.client.uuid,
                                                           touch.uuid,
                                                           [NSNumber numberWithInt:touch.type],
                                                           [NSString stringWithFormat:@"%@",[formatter stringFromDate:touch.date]],
                                                           touch.status,
                                                           [NSNumber numberWithBool:touch.completed],
                                                           touch.note?touch.note:@"Completed"]
                                                 forKeys:@[@"client_id",
                                                           @"activity_id",
                                                           @"touch_id",
                                                           @"date",
                                                           @"status",
                                                           @"completed",
                                                           @"note"]];
    }
    else
    {
        parameters = [NSDictionary dictionaryWithObjects:@[
                                                           touch.client.uuid,
                                                           [NSNumber numberWithInt:touch.type],
                                                           [NSString stringWithFormat:@"%@",[formatter stringFromDate:touch.date]],
                                                           touch.status,
                                                           [NSNumber numberWithBool:touch.completed],
                                                           touch.note]
                                                 forKeys:@[@"client_id",
                                                           @"touch_id",
                                                           @"date",
                                                           @"status",
                                                           @"completed",
                                                           @"note"]];
    }
    
    //Perform the request
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer new]; // use an Json Serializer.
    if (PRODMODE)
    {
        manager.securityPolicy.allowInvalidCertificates = YES;
    }
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//         parse response
//        NSLog(@"response %@", responseObject);
//        
//        NSDictionary* dictionaryResponse = (NSDictionary *)responseObject;
//        if([[dictionaryResponse objectForKey:@"responseCode"] intValue]==200)
//        {
//            failureBlock();
//        }
//        else
//        {
//            NSDictionary* dictionaryData = [dictionaryResponse objectForKey:@"data"];
//            Touch *touch =   [Touch initWithUpdateDictionary:dictionaryData];
//            if([[DatabaseManager.instance touchWithUuid:touch.uuid] uuid]) //Touch already existed on the DB
//            {
//                [DatabaseManager.instance deleteTouch:touch];
//                
//                touch.name = [RequestManager activityForTouchType:touch.type];
//                [DatabaseManager.instance insertTouch:touch];
//                successBlock(touch);
//            }
//            else // Touch is new in the DB
//            {
//                touch.name = @"Intended Activity";
//                [DatabaseManager.instance insertTouch:touch];
//                successBlock(touch);
//            }
//        }
         successBlock(touch);
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // inform error.
              failureBlock();
          }];

}

+(void) registerOrUpdateTouch:(Touch*)touch withSuccessBlock:(void(^)(Touch* touch))successBlock failureBlock:(void(^)(void))failureBlock
{
    //Create Url and parameters
    NSString * url = [NSString stringWithFormat:@"%@/activities",BaseURLString];
    NSDictionary *parameters;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSInteger touchType=(touch.type==0)?touch.customType:touch.type;
    if(touch.uuid)
    {
        parameters = [NSDictionary dictionaryWithObjects:@[
                                                           touch.client.uuid,
                                                           touch.uuid,
                                                           [NSNumber numberWithInteger:touchType],
                                                           [NSString stringWithFormat:@"%@",[formatter stringFromDate:touch.date]],
                                                           touch.status,
                                                           [NSNumber numberWithBool:touch.completed],
                                                           touch.note?touch.note:@""]
                      
                                                 forKeys:@[@"client_id",
                                                           @"activity_id",
                                                           @"touch_id",
                                                           @"date",
                                                           @"status",
                                                           @"completed",
                                                           @"note",
                                                           ]];
    }
    else
    {
        parameters = [NSDictionary dictionaryWithObjects:@[
                                                           touch.client.uuid,
                                                           [NSNumber numberWithInteger:touchType],
                                                           [NSString stringWithFormat:@"%@",[formatter stringFromDate:touch.date]],
                                                           touch.status,
                                                           [NSNumber numberWithBool:touch.completed],
                                                           touch.note]
                                                 forKeys:@[@"client_id",
                                                           @"touch_id",
                                                           @"date",
                                                           @"status",
                                                           @"completed",
                                                           @"note"]];
    }
    
    //Perform the request
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer new]; // use an Json Serializer.
    if (PRODMODE)
    {
        manager.securityPolicy.allowInvalidCertificates = YES;
    }
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
    // parse response
        NSLog(@"response %@", responseObject);
        
        NSDictionary* dictionaryResponse = (NSDictionary *)responseObject;
        if([[dictionaryResponse objectForKey:@"responseCode"] intValue]==200)
        {
            failureBlock();
        }
        else
        {
            NSDictionary* dictionaryData = [dictionaryResponse objectForKey:@"data"];
            Touch *touch =   [Touch initWithUpdateDictionary:dictionaryData];
            if([[DatabaseManager.instance touchWithUuid:touch.uuid] uuid]) //Touch already existed on the DB
            {
                [DatabaseManager.instance deleteTouch:touch];
                
                touch.name = [RequestManager activityForTouchType:touch.type];
                [DatabaseManager.instance insertTouch:touch];
                successBlock(touch);
            }
            else // Touch is new in the DB
            {
                touch.name = @"Intended Activity";
                [DatabaseManager.instance insertTouch:touch];
                successBlock(touch);
            }
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // inform error.
              failureBlock();
          }];
}

+(void)deleteTouch:(Touch*)touch withSuccessBlock:(void(^)(void))successBlock failureBlock:(void(^)(void))failureBlock
{
    // 1
    NSString *string = [NSString stringWithFormat:@"%@/activities/%@", BaseURLString,touch.uuid];
    //NSURL *url = [NSURL URLWithString:string];
    //Perform the request
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer new]; // use an Json Serializer.
    if (PRODMODE)
    {
        manager.securityPolicy.allowInvalidCertificates = YES;
    }
   
    [manager DELETE:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // parse response
        
        NSDictionary* dictionaryResponse = (NSDictionary *)responseObject;
        if([[dictionaryResponse objectForKey:@"responseCode"] intValue]==100)
        {
            successBlock();
        }
        else
        {
            failureBlock();
            
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              // inform error.
              failureBlock();
          }];
}

+(NSString *)activityForTouchType:(TouchType)touchType
{
    NSArray *array =  [[DatabaseManager instance] getAllMasterTouches];
    for (MasterTouch *mTouch in array)
    {
        if (mTouch.touch_id == touchType)
        {
            return mTouch.title;
        }
    }

    return @"CUSTOM";
}

@end






