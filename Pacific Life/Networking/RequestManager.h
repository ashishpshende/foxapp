//
//  RequestManager.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "DatabaseManager.h"
#import "Touch.h"
typedef void (^BBFailureBlock)(NSString *,NSError *);
@interface RequestManager : NSObject
+(void) loginForUser:(NSString*)username password:(NSString*)password andCompletionBlock:(void(^)(BOOL success, NSString* errorDescription, NSError *error))loginCompletionBlock;
+(void) logoutWithCompletionBlock:(void(^)(BOOL success, NSError *error))logoutCompletionBlock;
+(void)allActiveClientsSuccessBLock:(void(^)(NSArray* clients))successBlock failureBlock:(void(^)(void))failureBlock;
+(void)allTouchesForClient:(Client*)client andSuccessBlock:(void(^)(NSArray* touches))successBlock failureBlock:(void(^)(void))failureBlock;
+(void)updateProcessWithSuccessBlock:(void(^)(void))successBlock failureBlock:(void(^)(void))failureBlock;
+(void) registerOrUpdateClient:(Client*)client withSuccessBlock:(void(^)(Client* client))successBlock failureBlock:(void(^)(void))failureBlock;
+(void) registerOrUpdateTouch:(Touch*)touch withSuccessBlock:(void(^)(Touch* touch))successBlock failureBlock:(void(^)(void))failureBlock;
+(void) completeTouch:(Touch*)touch withSuccessBlock:(void(^)(Touch* touch))successBlock failureBlock:(void(^)(void))failureBlock;
+(void)deleteTouch:(Touch*)touch withSuccessBlock:(void(^)(void))successBlock failureBlock:(void(^)(void))failureBlock;
+(void) syncTouchesForClient:(Client*)client withSuccessBlock:(void(^)(NSArray* touches))successBlock failureBlock:(void(^)(void))failureBlock;
@end
