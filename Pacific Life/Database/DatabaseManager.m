//
//  DatabaseManager.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "DatabaseManager.h"
#import "Touch.h"
#include <stdlib.h>

@implementation DatabaseManager

+ (DatabaseManager *)instance
{
    static DatabaseManager *instance;
    @synchronized(self) {
        if(!instance) {
            instance = [[DatabaseManager alloc] init];
        }
    }
    return instance;
}

-(FMDatabase*)connectToDB
{
    if(!_database)
    {
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:@"PacificLife.sqlite"];
        _database = [FMDatabase databaseWithPath:databasePath];
    }
    [_database open];
    return _database;
    
}


-(BOOL)closeDB
{
    //return [_database close];
    return YES;
}

// Selects

-(Client*)clientWithUuid:(NSNumber*)uuid
{
    FMResultSet *clientTouch =[_database executeQuery:@"select * from Client where uuid = ?",uuid];
    Client *client = [Client new];
    while ([clientTouch next])
    {
        client.uuid = [NSNumber numberWithInt:[clientTouch intForColumn:@"uuid"]];
        client.sellerId = [NSNumber numberWithInt:[clientTouch intForColumn:@"sellerId"]];
        client.name = [clientTouch stringForColumn:@"name"];
        client.status = [clientTouch stringForColumn:@"status"];
        client.email = [clientTouch stringForColumn:@"email"];
        client.company = [clientTouch stringForColumn:@"company"];
        client.phone = [clientTouch stringForColumn:@"phone"];
        client.address = [clientTouch stringForColumn:@"address"];
        client.state = [clientTouch stringForColumn:@"state"];
        client.city = [clientTouch stringForColumn:@"city"];
        client.zip = [clientTouch stringForColumn:@"zip"];
        client.rating = [clientTouch stringForColumn:@"rating"];
        client.isActive = [clientTouch intForColumn:@"isActive"]==1?YES:NO;
        client.createdAt = [clientTouch dateForColumn:@"createdAt"];
        client.updatedAt = [clientTouch dateForColumn:@"updatedAt"];
    }
    return client;
}

-(NSArray*)allClients
{
    FMResultSet *clientTouch =[_database executeQuery:@"select * from Client"];
    NSMutableArray *clients = [NSMutableArray new];
    
    while ([clientTouch next])
    {
        Client *client = [Client new];
        client.uuid = [NSNumber numberWithInt:[clientTouch intForColumn:@"uuid"]];
        client.sellerId = [NSNumber numberWithInt:[clientTouch intForColumn:@"sellerId"]];
        client.name = [clientTouch stringForColumn:@"name"];
        client.status = [clientTouch stringForColumn:@"status"];
        client.email = [clientTouch stringForColumn:@"email"];
        client.company = [clientTouch stringForColumn:@"company"];
        client.phone = [clientTouch stringForColumn:@"phone"];
        client.address = [clientTouch stringForColumn:@"address"];
        client.state = [clientTouch stringForColumn:@"state"];
        client.city = [clientTouch stringForColumn:@"city"];
        client.zip = [clientTouch stringForColumn:@"zip"];
        client.rating = [clientTouch stringForColumn:@"rating"];
        client.isActive = [clientTouch intForColumn:@"isActive"]==1?YES:NO;
        client.createdAt = [clientTouch dateForColumn:@"createdAt"];
        client.updatedAt = [clientTouch dateForColumn:@"updatedAt"];
        [clients addObject:client];
    }
    return clients;
}

-(NSArray*)allArchiveClients
{
    FMResultSet *clientTouch =[_database executeQuery:@"select * from Client WHERE status = 'CW' OR status = 'CL'"];
    NSMutableArray *clients = [NSMutableArray new];
    
    while ([clientTouch next])
    {
        Client *client = [Client new];
        client.uuid = [NSNumber numberWithInt:[clientTouch intForColumn:@"uuid"]];
        client.sellerId = [NSNumber numberWithInt:[clientTouch intForColumn:@"sellerId"]];
        client.name = [clientTouch stringForColumn:@"name"];
        client.status = [clientTouch stringForColumn:@"status"];
        client.email = [clientTouch stringForColumn:@"email"];
        client.company = [clientTouch stringForColumn:@"company"];
        client.phone = [clientTouch stringForColumn:@"phone"];
        client.address = [clientTouch stringForColumn:@"address"];
        client.state = [clientTouch stringForColumn:@"state"];
        client.city = [clientTouch stringForColumn:@"city"];
        client.zip = [clientTouch stringForColumn:@"zip"];
        client.rating = [clientTouch stringForColumn:@"rating"];
        client.isActive = [clientTouch intForColumn:@"isActive"]==1?YES:NO;
        client.createdAt = [clientTouch dateForColumn:@"createdAt"];
        client.updatedAt = [clientTouch dateForColumn:@"updatedAt"];
        client.lastTouchDate = client.updatedAt;   //[self mostRecentTouchDateForClient:client];
        [clients addObject:client];
    }
    return clients;
}

-(void)deleteAllDataFromTables
{
    [_database executeUpdate:@"DELETE from Client"];
    [_database executeUpdate:@"DELETE from Touch"];
    [_database executeUpdate:@"DELETE from MasterTouches"];
}
-(void)deleteAllDataFromTouchTable
{
    [_database executeUpdate:@"DELETE from Touch"];
    
}
-(void)deleteAllDataFromClientTable
{
    [_database executeUpdate:@"DELETE from Client"];
    
}
-(void)deleteAllDataFromMasterTouchTable
{
    [_database executeUpdate:@"DELETE from MasterTouches"];
    
}

-(void)deleteClient:(Client*)client andDeleteRelatedTouches:(BOOL)deleteTouches
{
    [_database executeUpdate:@"DELETE from Client where uuid = ?",client.uuid];
    if(deleteTouches)
    for(Touch *touch in [self touchesForClient:client])
    {
        [self deleteTouch:touch];
    }
    
}
-(NSArray *)getAllMasterTouches
{
    NSMutableArray *masterTouches = [NSMutableArray array];
    
    FMResultSet *resultTouch =[_database executeQuery:@"select * from MasterTouches order by id asc"];
    if (resultTouch)
    {
        while ([resultTouch next])
        {
            MasterTouch *mTouch = [[MasterTouch alloc]init];
            mTouch.touch_id =[resultTouch intForColumn:@"id"];
            mTouch.number =[resultTouch intForColumn:@"number"];
            mTouch.name = [resultTouch stringForColumn:@"name"];
            mTouch.note = [resultTouch stringForColumn:@"note"];
            mTouch.title = [resultTouch stringForColumn:@"title"];
            mTouch.company_touchid = [resultTouch intForColumn:@"cid"];
            [masterTouches addObject:mTouch];
        }
        
    }
    else
    {
        NSLog(@"ResultSet Empty");
    }
    
    return masterTouches;
}
-(NSArray *) getAllMasterids
{
    NSArray *array = [self getAllMasterTouches];
    NSMutableArray *idArray = [NSMutableArray array];
    for (MasterTouch *touch in array)
    {
        [idArray addObject:[NSNumber numberWithInteger:touch.touch_id]];
    }
    return idArray;
}
-(NSArray *) getAllTitles
{
    NSArray *array = [self getAllMasterTouches];
    NSMutableArray *titleArray = [NSMutableArray array];
    for (MasterTouch *touch in array)
    {
        [titleArray addObject:touch.title];
    }
    return titleArray;
}
-(void)deleteTouch:(Touch*)touch
{
    [_database executeUpdate:@"DELETE from Touch where uuid = ?",touch.uuid];
}
-(void)completeTouch:(Touch*)touch
{
    [_database executeUpdate:@"UPDATE Touch set completed = 1 where uuid = ?",touch.uuid];
}
-(Touch*)touchWithUuid:(NSNumber*)uuid
{
    FMResultSet *resultTouch =[_database executeQuery:@"select * from Touch where uuid = ?",uuid];
    Touch *touch = [Touch new];
    while ([resultTouch next])
    {
        touch.uuid = [NSNumber numberWithInt:[resultTouch intForColumn:@"uuid"]];
        touch.type = [resultTouch intForColumn:@"type"];
        touch.client = [self clientWithUuid:[NSNumber numberWithInt:[resultTouch intForColumn:@"clientUuid"]]];
        touch.date = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"date"]];
        touch.completedDate = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"completedDate"]];
        touch.name = [resultTouch stringForColumn:@"name"];
        touch.status = [resultTouch stringForColumn:@"status"];
        touch.note = [resultTouch stringForColumn:@"note"];
        touch.completed = [resultTouch intForColumn:@"completed"]==1?YES:NO;
        touch.createdAt = [resultTouch dateForColumn:@"createdAt"];
        touch.updatedAt = [resultTouch dateForColumn:@"updatedAt"];
    }
    return touch;
}

-(NSArray*)allFutureTouches
{
    NSDate *date = [NSDate date];
    FMResultSet *resultTouch =[_database executeQuery:@"select * from Touch WHERE completed = 0 AND status <>'CL' AND status<>'CW' order by date asc"];
    NSMutableArray *touches = [NSMutableArray new];
    if (resultTouch)
    {
        while ([resultTouch next ])
        {
            Touch *touch = [Touch new];
            touch.uuid = [NSNumber numberWithInt:[resultTouch intForColumn:@"uuid"]];
            touch.type = [resultTouch intForColumn:@"type"];
            touch.client = [self clientWithUuid:[NSNumber numberWithInt:[resultTouch intForColumn:@"clientUuid"]]];
            touch.date = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"date"]];
            touch.completedDate = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"completedDate"]];
            touch.name = [resultTouch stringForColumn:@"name"];
            touch.status = [resultTouch stringForColumn:@"status"];
            touch.note = [resultTouch stringForColumn:@"note"];
            touch.completed = [resultTouch intForColumn:@"completed"]==1?YES:NO;
            touch.createdAt = [resultTouch dateForColumn:@"createdAt"];
            touch.updatedAt = [resultTouch dateForColumn:@"updatedAt"];
            
            if(touch.completed)
            {
                touch.currentColor = Gray;
            }
            else
                if ([[NSDate date] compare:touch.date] == NSOrderedAscending) {
                    //NSLog(@"today is: %@",[NSNumber numberWithInt:[[NSDate date] timeIntervalSince1970]]);
                    NSInteger hours = [[[NSCalendar currentCalendar] components:NSHourCalendarUnit fromDate: [NSDate date]toDate:touch.date options:0] hour];
                    if(hours <= 24)
                    {
                        touch.currentColor = Red;
                    }
                    else if((hours > 24)&&(hours<(24*7)))
                    {
                        touch.currentColor = Yellow;
                    }
                    else if(hours>(24*7))
                    {
                        touch.currentColor = Green;
                    }
                }
                else
                {
                    touch.currentColor = Red;
                }
            
            NSArray *mtouches = [[DatabaseManager instance] getAllMasterTouches];
            
            for (MasterTouch *mTouch in mtouches)
            {
                if(mTouch.company_touchid == touch.type)
                    touch.type = mTouch.touch_id;
            }
            
            [touches addObject:touch];
        }

    }
    else
    {
        
    }
    
    return touches;

}

-(NSArray*)allTouches
{
    NSDate *date = [NSDate date];
    FMResultSet *resultTouch =[_database executeQuery:@"select * from Touch order by date asc",date];
    NSMutableArray *touches = [NSMutableArray new];
    while ([resultTouch next ])
    {
        Touch *touch = [Touch new];
        touch.uuid = [NSNumber numberWithInt:[resultTouch intForColumn:@"uuid"]];
        touch.type = [resultTouch intForColumn:@"type"];
        touch.client = [self clientWithUuid:[NSNumber numberWithInt:[resultTouch intForColumn:@"clientUuid"]]];
        touch.date = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"date"]];
        touch.completedDate = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"completedDate"]];
        touch.name = [resultTouch stringForColumn:@"name"];
        touch.status = [resultTouch stringForColumn:@"status"];
        touch.note = [resultTouch stringForColumn:@"note"];
        touch.completed = [resultTouch intForColumn:@"completed"]==1?YES:NO;
        touch.createdAt = [resultTouch dateForColumn:@"createdAt"];
        touch.updatedAt = [resultTouch dateForColumn:@"updatedAt"];
        if(touch.completed)
        {
            touch.currentColor = Gray;
        }
        else
        if ([[NSDate date] compare:touch.date] == NSOrderedAscending) {
            //NSLog(@"today is: %@",[NSNumber numberWithInt:[[NSDate date] timeIntervalSince1970]]);
            NSInteger hours = [[[NSCalendar currentCalendar] components:NSHourCalendarUnit fromDate: [NSDate date]toDate:touch.date options:0] hour];
                if(hours <= 24)
                {
                    touch.currentColor = Red;
                }
                else if((hours > 24)&&(hours<(24*7)))
                {
                    touch.currentColor = Yellow;
                }
                else if(hours>(24*7))
                {
                    touch.currentColor = Green;
                }
        }
        else
        {
            touch.currentColor = Red;
        }
        [touches addObject:touch];
    }
    return touches;
    
}

-(NSArray*)touchesForClient:(Client*)client
{
    FMResultSet *resultTouch =[_database executeQuery:@"select * from Touch where clientUuid = ? AND (status !='CW' OR status !='CL') order by completed desc, date asc",client.uuid];
    NSMutableArray *touches = [NSMutableArray new];
    while ([resultTouch next ])
    {
        Touch *touch = [Touch new];
        touch.uuid = [NSNumber numberWithInt:[resultTouch intForColumn:@"uuid"]];
        touch.type = [resultTouch intForColumn:@"type"];
        touch.client = [self clientWithUuid:[NSNumber numberWithInt:[resultTouch intForColumn:@"clientUuid"]]];
        touch.date = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"date"]];
        touch.completedDate = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"completedDate"]];
        touch.name = [resultTouch stringForColumn:@"name"];
        touch.status = [resultTouch stringForColumn:@"status"];
        touch.note = [resultTouch stringForColumn:@"note"];
        touch.completed = [resultTouch intForColumn:@"completed"]==1?YES:NO;
        touch.createdAt = [resultTouch dateForColumn:@"createdAt"];
        touch.updatedAt = [resultTouch dateForColumn:@"updatedAt"];
        
        if(touch.completed)
        {
            touch.currentColor = Gray;
        }
        else
        if ([[NSDate date] compare:touch.date] == NSOrderedAscending)
        {
        
            NSInteger hours = [[[NSCalendar currentCalendar] components:NSHourCalendarUnit fromDate: [NSDate date]toDate:touch.date options:0] hour];
                if(hours <= 24)
                {
                    touch.currentColor = Red;
                }
                else if((hours > 24)&&(hours<(24*7)))
                {
                    touch.currentColor = Yellow;
                }
                else if(hours>(24*7))
                {
                    touch.currentColor = Green;
                }
        }
        else
        {
            touch.currentColor = Red;
        }
        
        
        NSArray *mtouches = [[DatabaseManager instance] getAllMasterTouches];
        
        for (MasterTouch *mTouch in mtouches)
        {
            if(mTouch.company_touchid == touch.type)
            {
                touch.company_touchid = touch.type;
                touch.mTouchId = mTouch.touch_id;
                touch.type = mTouch.touch_id;
            }
            
        }
        
        [touches addObject:touch];
    }
    
    
    return touches;

}

-(NSDate*)mostRecentTouchDateForClient:(Client*)client
{
    FMResultSet *resultTouch =[_database executeQuery:@"select * from Touch where clientUuid = ? order by date desc",client.uuid];
    Touch *touch = [Touch new];
    if ([resultTouch next ])
    {
        
        touch.uuid = [NSNumber numberWithInt:[resultTouch intForColumn:@"uuid"]];
        touch.type = [resultTouch intForColumn:@"type"];
        touch.client = [self clientWithUuid:[NSNumber numberWithInt:[resultTouch intForColumn:@"clientUuid"]]];
        touch.date = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"date"]];
        touch.completedDate = [NSDate dateWithTimeIntervalSince1970:[resultTouch intForColumn:@"completedDate"]];
        touch.name = [resultTouch stringForColumn:@"name"];
        touch.status = [resultTouch stringForColumn:@"status"];
        touch.note = [resultTouch stringForColumn:@"note"];
        touch.completed = [resultTouch intForColumn:@"completed"]==1?YES:NO;
        touch.createdAt = [resultTouch dateForColumn:@"createdAt"];
        touch.updatedAt = [resultTouch dateForColumn:@"updatedAt"];
        
        if ([[NSDate date] compare:touch.date] == NSOrderedAscending) {
            //NSLog(@"today is: %@",[NSNumber numberWithInt:[[NSDate date] timeIntervalSince1970]]);
            NSInteger hours = [[[NSCalendar currentCalendar] components:NSHourCalendarUnit fromDate: [NSDate date]toDate:touch.date options:0] hour];
            if(touch.completed)
            {
                touch.currentColor = Gray;
            }
            else
                if(hours <= 24)
                {
                    touch.currentColor = Red;
                }
                else if((hours > 24)&&(hours<(24*7)))
                {
                    touch.currentColor = Yellow;
                }
                else if(hours>(24*7))
                {
                    touch.currentColor = Green;
                }
        }
        else
        {
            touch.currentColor = Red;
        }
        
        
        //[touches addObject:touch];
    }
    return touch.date;
    
}

// Inserts
-(void)insertMasterTouch:(MasterTouch*)mTouch
{
    if ([_database executeUpdate:@"INSERT into MasterTouches (id, name, note, title, number, cid) values (?, ?, ?, ?, ?, ?)",
         [NSNumber numberWithInteger:mTouch.touch_id],mTouch.name,mTouch.note,mTouch.title,
         [NSNumber numberWithInteger:mTouch.number],[NSNumber numberWithLong:	mTouch.company_touchid]
         ])
    {
        NSLog(@"Master Touch Inserted.");
    }
    else
    {
        NSLog(@"Unable to save Master Touch %@.",mTouch.name);
    }
    
}
-(void)insertClient:(Client*)client
{
    
    [_database executeUpdate:@"INSERT OR REPLACE into Client (uuid, sellerId, name, status, email, company, phone, address, state, city, zip, rating, isActive, createdAt, updatedAt) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", client.uuid,client.sellerId, client.name, client.status, client.email, client.company, client.phone, client.address, client.state, client.city, client.zip, client.rating, [NSNumber numberWithBool:client.isActive], [NSNumber numberWithInt:[client.createdAt timeIntervalSince1970]], [NSNumber numberWithInt:[client.updatedAt timeIntervalSince1970]]];
}

-(void)insertTouch:(Touch*)touch
{
//    NSArray *mTouches = [self getAllMasterTouches];
//    BOOL flag = NO;
//    for (MasterTouch *mTouch in mTouches)
//    {
//        if (touch.type == mTouch.touch_id)
//        {
//            flag = YES;
//            break;
//        }
//    }
//    
//    if (!flag)
//    {
//        touch.type = touch.mTouchId;
//    }
    
    
   // touch.type = touch.mTouchId;
    [_database executeUpdate:@"INSERT OR REPLACE into Touch (uuid, completed, name, status, createdAt, updatedAt, clientUuid, type, date, note) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", touch.uuid, [NSNumber numberWithBool:touch.completed], touch.name, touch.status?touch.status:@"",[NSNumber numberWithInt:[touch.createdAt timeIntervalSince1970]], [NSNumber numberWithInt:[touch.updatedAt timeIntervalSince1970]], touch.client.uuid, [NSNumber numberWithInt:touch.type], [NSNumber numberWithInt:[touch.date timeIntervalSince1970]], touch.note?touch.note:@""];
}

// Updates
-(void)changeTouchType:(int)touchType toTouch:(Touch*)touch
{
    [_database executeUpdate:@"UPDATE Touch SET type = ? WHERE uuid = ?",[NSNumber numberWithInt:touchType], touch.uuid];
}

-(void)changeDate:(NSDate*)date toTouch:(Touch*)touch
{
    [_database executeUpdate:@"UPDATE Touch SET date = ? WHERE uuid = ?",[NSNumber numberWithInt:[date timeIntervalSince1970]], touch.uuid];
}

-(void)changeCompletedDate:(NSDate*)completedDate toTouch:(Touch*)touch
{
    
    [_database executeUpdate:@"UPDATE Touch SET completedDate = ?, completed = ? WHERE uuid = ?",[NSNumber numberWithInt:[completedDate timeIntervalSince1970]],[NSNumber numberWithInt:1], touch.uuid];
}

-(void)changeActivity:(NSString*)activity toTouch:(Touch*)touch
{
    [_database executeUpdate:@"UPDATE Touch SET name = ? WHERE uuid = ?",activity, touch.uuid];
}
-(void)changeNote:(NSString*)note toTouch:(Touch*)touch
{
    [_database executeUpdate:@"UPDATE Touch SET note = ? WHERE uuid = ?",note, touch.uuid];
}
-(void) saveMasterTouches
{
    
}

@end
