//
//  DatabaseManager.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "Client.h"
#import "MasterTouch.h"
@class Touch;
@interface DatabaseManager : NSObject
@property (nonatomic,strong) FMDatabase *database;
+ (DatabaseManager *)instance;
-(FMDatabase*)connectToDB;
-(NSArray*)allClients;
-(NSArray*)allArchiveClients;
-(NSArray*)allTouches;
-(NSArray*)allFutureTouches;
-(NSArray*)touchesForClient:(Client*)client;
-(NSDate*)mostRecentTouchDateForClient:(Client*)client;

-(void)changeTouchType:(int)touchType toTouch:(Touch*)touch;
-(void)changeDate:(NSDate*)date toTouch:(Touch*)touch;
-(void)changeActivity:(NSString*)activity toTouch:(Touch*)touch;
-(void)changeNote:(NSString*)note toTouch:(Touch*)touch;

-(void)changeCompletedDate:(NSDate*)completedDate toTouch:(Touch*)touch;

-(void)insertMasterTouch:(MasterTouch*)mTouch;
-(void)insertClient:(Client*)client;
-(void)insertTouch:(Touch*)touch;
-(Client*)clientWithUuid:(NSNumber*)uuid;
-(Touch*)touchWithUuid:(NSNumber*)uuid;
-(void)deleteClient:(Client*)client andDeleteRelatedTouches:(BOOL)deleteTouches;
-(void)deleteTouch:(Touch*)touch;
-(void)completeTouch:(Touch*)touch;
-(NSArray *)getAllMasterTouches;
-(NSArray *) getAllTitles;
-(NSArray *) getAllMasterids;
-(void)deleteAllDataFromTables;

-(void)deleteAllDataFromTouchTable;
-(void)deleteAllDataFromClientTable;
-(void)deleteAllDataFromMasterTouchTable;
@end
