//
//  LoginVC.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 27/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestManager.h"
#import "MBProgressHUD.h"
@interface LoginVC : UIViewController
- (IBAction)loginAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)forgotYourPasswordAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;

@end
