//
//  NewClientVC.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 27/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionSheetPicker.h"
#import "Client.h"
#import "TouchListTVC.h"
#import "RequestManager.h"
#import "MBProgressHUD.h"
@interface NewClientVC : UIViewController

@property (strong,nonatomic) MBProgressHUD *mbProgressHUD;
@property (nonatomic,assign) BOOL creatingNew;
@property (nonatomic,strong) NSArray *statesNames;
@property (nonatomic,strong) NSArray *statesAbbreviation;
@property (strong,nonatomic) Client *client;

@property (weak, nonatomic) IBOutlet UIButton *cancelOrBackButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
- (IBAction)saveAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeRatingButton;


@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *companyTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipTextField;
@property (weak, nonatomic) IBOutlet UIButton *changeStateButton;

@property (weak, nonatomic) IBOutlet UIButton *actionButton;

- (IBAction)createClientAction:(id)sender;

@end
