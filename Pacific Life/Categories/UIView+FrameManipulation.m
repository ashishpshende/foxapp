//
//  UIView+FrameManipulation.m
//  Utilities
//
//  Created by Rodrigo Larrosa on 3/13/14.
//  Copyright (c) 2014 Rodrigo Larrosa. All rights reserved.

//  This category provides you with methods to access and manipulate a view frame
//  Access or change the size, width, x and y coordinate with a simpler sintaxis and less code
//  Simpler access to the corner coordinates (topY, bottomY, leftX and rightX)
//  Easily print the view frame or get a string representation of it

#import "UIView+FrameManipulation.h"

@implementation UIView (FrameManipulation)

#pragma mark - Frame position

-(float)x{
    
    return self.frame.origin.x;
}

-(void)setX:(float)x{
    
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

-(float)y{
    
    return self.frame.origin.y;
}

-(void)setY:(float)y{
    
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

-(CGPoint)origin{
    
    return self.frame.origin;
}

-(void)setOrigin:(CGPoint)origin{
    
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}


#pragma mark - Width and Height

-(float)width{
    
    return self.frame.size.width;
}

-(void)setWidth:(float)width{
    
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

-(float)height{
    
    return self.frame.size.height;
}

-(void)setHeight:(float)height{
    
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

-(CGSize)size{
    
    return self.frame.size;
}

-(void)setSize:(CGSize)size{
    
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

#pragma mark - Min and Max Coordinates

-(float)topY{
    
    return CGRectGetMinY(self.frame);
}

-(float)bottomY{
    
    return CGRectGetMaxY(self.frame);
}

-(float)leftX{
    
    return CGRectGetMinX(self.frame);
}

-(float)rightX{
    
    return CGRectGetMaxX(self.frame);
}

#pragma mark -

//Returns a string representation of the view´s frame
-(NSString*)stringFrame{

    return NSStringFromCGRect(self.frame);
}

//Print the view´s frame
-(void)printFrame{
    
    NSLog(@"Frame:%@",NSStringFromCGRect(self.frame));
}


@end
