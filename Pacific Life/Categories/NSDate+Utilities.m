//
//  NSDate+Utilities.m
//  Utilities
//
//  Created by Rodrigo Larrosa on 4/15/14.
//  Copyright (c) 2014 Rodrigo Larrosa. All rights reserved.
//

#import "NSDate+Utilities.h"

@implementation NSDate (Utilities)

#define CURRENT_CALENDAR [NSCalendar currentCalendar]

#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)



#pragma mark - Date from String

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format {
    
    if ((NSNull *)string == [NSNull null] || string == nil) {
        return nil;
    }
    
    if (format == nil) {
        format = @"yyyy-MM-dd";
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    NSDate *result = [formatter dateFromString:string];
    
    return result;
}

// Date from a string with "yyyy-MM-dd'T'HH:mm:ssZZZ" format
+ (NSDate *)dateFromCompleteDateString:(NSString *)string {
    NSDate *result = [NSDate dateFromString:string withFormat:@"yyyy-MM-dd HH:mm:ss"];
    return result;
}

// Date from a string with "yyyy-MM-dd'T'HH:mm:ssZZZ" format
+ (NSDate *)dateFromShortDateString:(NSString *)string {
    NSDate *result = [NSDate dateFromString:string withFormat:@"yyyy-MM-dd"];
    return result;
}

#pragma mark - Date components

- (NSInteger)hour
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components hour];
}

- (NSInteger)minute
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components minute];
}

- (NSInteger)seconds
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components second];
}

- (NSInteger)day
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components day];
}

- (NSInteger)month
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components month];
}

- (NSInteger)week
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components week];
}

- (NSInteger)year
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	return [components year];
}


- (NSString*)monthName {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM"];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSString *stringFromDate = [formatter stringFromDate:self];
    return stringFromDate;
}

#pragma mark - Date with components

- (NSDate*)dateWithSecond:(NSInteger)second{
    
    NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
    [components setSecond:second];
    
    return [CURRENT_CALENDAR dateFromComponents:components];
}

- (NSDate*)dateWithHour:(NSInteger)hour minute:(NSInteger)minute{
    
    NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
    [components setHour:hour];
    [components setMinute:minute];
    
    return [CURRENT_CALENDAR dateFromComponents:components];
}

- (NSDate*)dateWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year{
    
    NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
    [components setDay:day];
    [components setMonth:month];
    [components setYear:year];
    
    return [CURRENT_CALENDAR dateFromComponents:components];
}

- (NSDate*)dateWithMinute:(NSInteger)minute hour:(NSInteger)hour day:(NSInteger)day month:(NSInteger)month year:(NSInteger)year{
    
    NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
    [components setMinute:minute];
    [components setHour:hour];
    [components setDay:day];
    [components setMonth:month];
    [components setYear:year];
    
    return [CURRENT_CALENDAR dateFromComponents:components];
}

- (NSDate*)dateWithTimeZone:(NSTimeZone*)timeZone{
    
    NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
    [components setTimeZone:timeZone];
    
    return [CURRENT_CALENDAR dateFromComponents:components];
}

@end
