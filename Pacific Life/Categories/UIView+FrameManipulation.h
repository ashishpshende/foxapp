//
//  UIView+FrameManipulation.h
//  Utilities
//
//  Created by Rodrigo Larrosa on 3/13/14.
//  Copyright (c) 2014 Rodrigo Larrosa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FrameManipulation)

@property float x;
@property float y;
@property CGPoint origin;

@property float width;
@property float height;
@property CGSize size;

@property (readonly) float topY;
@property (readonly) float bottomY;
@property (readonly) float leftX;
@property (readonly) float rightX;

-(NSString*)stringFrame;
-(void)printFrame;

@end
