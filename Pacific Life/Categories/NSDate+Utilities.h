//
//  NSDate+Utilities.h
//  Utilities
//
//  Created by Rodrigo Larrosa on 4/15/14.
//  Copyright (c) 2014 Rodrigo Larrosa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utilities)

#pragma mark - Date from String

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format;

// Date from a string with "yyyy-MM-dd'T'HH:mm:ssZZZ" format
+ (NSDate *)dateFromCompleteDateString:(NSString *)string;

// Date from a string with "yyyy-MM-dd'T'HH:mm:ssZZZ" format
+ (NSDate *)dateFromShortDateString:(NSString *)string;

#pragma mark - Date components

- (NSInteger)seconds;
- (NSInteger)minute;
- (NSInteger)hour;
- (NSInteger)day;
- (NSInteger)month;
- (NSInteger)week;
- (NSInteger)year;
- (NSString*)monthName;

#pragma mark - Date with components

- (NSDate*)dateWithSecond:(NSInteger)second;
- (NSDate*)dateWithHour:(NSInteger)hour minute:(NSInteger)minute;
- (NSDate*)dateWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;
- (NSDate*)dateWithMinute:(NSInteger)minute hour:(NSInteger)hour day:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;
- (NSDate*)dateWithTimeZone:(NSTimeZone*)timeZone;

@end
