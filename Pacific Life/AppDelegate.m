//
//  AppDelegate.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 14/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkActivityLogger.h"
#import "Touch.h"
#import "RequestManager.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [[UIScreen mainScreen] bounds].size.height
    // Override point for customization after application launch.
    [self copyDbToDocumentsFolder];
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    
    [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
    //[DatabaseManager.instance createDummyContentTouches];
    
    NSLog(@"Requesting for Push Notification APNS Token");
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self updateBadgeCount];
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
   
    NSString* deviceTokenStr = [[[[deviceToken description]
                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""];
  
    NSLog(@"Received Push Notification APNS Token :  %@",deviceTokenStr);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:deviceTokenStr forKey:@"apns"];
    [defaults synchronize];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    
    if (error.code == 3010)
    {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ" forKey:@"apns"];
        [defaults synchronize];
    } else
    {
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error.localizedDescription);
    }
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
     NSLog(@"Notification Received : %@",userInfo);
    NSDictionary *resp = [userInfo objectForKey:@"aps"];
    UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:[resp objectForKey:@"alert"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [RequestManager updateProcessWithSuccessBlock:^{
        //
        [self updateBadgeCount];
    } failureBlock:^{
        //
        [self updateBadgeCount];
    }];
      [self updateBadgeCount];
}
-(void) updateBadgeCount
{
    NSInteger badgeCount=0;
    NSArray  *touches = [[DatabaseManager instance] allFutureTouches];
    for (Touch *touch in touches)
    {
        NSInteger hours = [[[NSCalendar currentCalendar] components:NSHourCalendarUnit fromDate: [NSDate date]toDate:touch.date options:0] hour];
        if(hours <= 24)
        {
            badgeCount ++;
        }
    }
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BadgeCount Test" message:[NSString stringWithFormat:@"BadgeCount %ld",(long)badgeCount] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self updateBadgeCount];
    
}
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     [self updateBadgeCount];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)copyDbToDocumentsFolder
{
    BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"PacificLife.sqlite"];
	success = [fileManager fileExistsAtPath:writableDBPath];
	if (success){
        [[DatabaseManager instance] connectToDB];
        return;
    }
	NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"PacificLife.sqlite"];
	success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
	if (!success) {
		NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
	}
    else
    {
        [[DatabaseManager instance] connectToDB];
    }
}

@end
