//
//  ArchiveClientCell.h
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 2/06/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Client.h"
#import "SWTableViewCell.h"

@interface ArchiveClientCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *clientRowNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *clientNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconIndicatorImage;
@property (weak, nonatomic) IBOutlet UILabel *iconIndicatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
-(void)setupCellForClient:(Client*)client andIndexPath:(NSIndexPath*)indexPath;
@end
