//
//  TouchListTVC.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 26/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "TouchListTVC.h"

@interface TouchListTVC ()
{
    NSInteger completedTouchesCount;
}
@end

@implementation TouchListTVC

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
} 

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_theTableView setBackgroundColor:[UIColor clearColor]];
    _theTableView.tableFooterView  = [[UIView alloc]initWithFrame:CGRectZero];
    [self refreshTable];
}
-(void) reloadDataFromLocalDatabase
{
  _touches = (NSMutableArray *) [DatabaseManager.instance touchesForClient:_client];
    [_theTableView reloadData];
}
-(void)loadData
{
    _grayCount = 0;
    _clientName.text = _client.name;
    _touches = (NSMutableArray *) [DatabaseManager.instance touchesForClient:_client];
    [_theTableView setSeparatorInset:UIEdgeInsetsZero];
    [_theTableView reloadData];
    _formatter = [[NSDateFormatter alloc] init];
    [_formatter setDateFormat:@"MM/dd/yyyy"];
    [_formatter setLocale:[NSLocale systemLocale]];
    _grayCount = [self countOfGrays];
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:_grayCount inSection:0];
    [_theTableView scrollToRowAtIndexPath:myIP atScrollPosition:UITableViewScrollPositionTop animated:NO];
   
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    _refreshControl = [[TableRefreshControl alloc]init];
    [self.theTableView addSubview:_refreshControl];
      [self.theTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.theTableView.tableFooterView setFrame:CGRectZero];
    [_refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];

}
- (void)refreshTable
{
    MBProgressHUD *mbProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    mbProgressHUD.labelText = @"Loading...";
    [RequestManager updateProcessWithSuccessBlock:^{
        [_refreshControl endRefreshing];
        [self loadData];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        completedTouchesCount = 0;
            [self.theTableView reloadData];
    } failureBlock:^{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [_refreshControl endRefreshing];
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    _touches =(NSMutableArray *) [DatabaseManager.instance touchesForClient:_client];
    _grayCount = [self countOfGrays];
    int numberOfNonGrray = (int)_touches.count-_grayCount;
    int numberOfCells = 0;
    
    if(numberOfNonGrray<7)
    {
        if(_grayCount>0)
        {
            numberOfCells = 7 + _grayCount;
        }
        else
            numberOfCells = 7;
        
    }
    else
    {
        numberOfCells = (int) _touches.count;
    }

    return numberOfCells;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TouchListCell *cell;
    
    if(indexPath.row>=_touches.count)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"touchCellWhite" forIndexPath:indexPath];
    }
    else
    {
        Touch *touch = [_touches objectAtIndex:indexPath.row];
        if(touch.currentColor==Gray)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"touchCellGray" forIndexPath:indexPath];
        }
        else if(touch.currentColor==Red)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"touchCellRed" forIndexPath:indexPath];
        }
        else if(touch.currentColor==Yellow)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"touchCellYellow" forIndexPath:indexPath];
        }
        else if(touch.currentColor==Green)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"touchCellGreen" forIndexPath:indexPath];
        }
        if (touch.completed)
        {
            completedTouchesCount ++;
        }
        [cell setupCellForTouch:touch andFormatter:_formatter];
        cell.touchNumberLabel.text = [NSString stringWithFormat:@"%i",(int)(indexPath.row+1)];
        cell.tableDelegate = self;
        cell.delegate = self;
    }
    
    UIEdgeInsets insets = cell.separatorInset;
    cell.separatorInset = UIEdgeInsetsMake(0.0, insets.left + 1.0, 0.0, 0.0);
    cell.separatorInset = insets;
    cell.moreInfoButton.tag = indexPath.row;
    
    return cell;
}

#pragma mark - SWTableViewCell options menu methods
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    //[cell hideUtilityButtonsAnimated:YES];
    NSLog(@"Cell Scrolled");
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSLog(@"%ld",(long)index);
     NSIndexPath *cellIndexPath = [self.theTableView indexPathForCell:cell];
    Touch *selectedTouch = [_touches objectAtIndex:cellIndexPath.row];
    if (index == 0)
    {

        if (selectedTouch.completed)
        {
            
            [self deleteTouch:selectedTouch];
            
        }
        else
        {
            [self completeTouch:selectedTouch];
        }
    }
    else
    {
            [self deleteTouch:selectedTouch];
    }
}
-(void) completeTouch:(Touch *)touch
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    touch.completedDate = [NSDate date];
    touch.type = touch.company_touchid;
    [RequestManager completeTouch:touch withSuccessBlock:^(Touch *touch) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[DatabaseManager instance] completeTouch:touch];
        [self refreshTable];
    } failureBlock:^{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }];
}
-(void) deleteTouch:(Touch *)touch
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    touch.type = touch.company_touchid;
    [RequestManager deleteTouch:touch withSuccessBlock:^{
        //
        [DatabaseManager.instance deleteTouch:touch];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        _touches = (NSMutableArray *) [DatabaseManager.instance touchesForClient:_client];
        NSInteger deletedIndex = -1;
        
        for (Touch *cTouch in _touches)
        {
            if (cTouch.uuid == touch.uuid)
            {
                deletedIndex = [_touches indexOfObject:cTouch];
            }
        }
 
        @try
        {
              [_touches removeObjectAtIndex:deletedIndex];
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception: %@",exception);
        }
        @finally
        {
            
        }
        
        completedTouchesCount = 0;
        [self.theTableView reloadData];
        [self.theTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:completedTouchesCount inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    } failureBlock:^{
        // something went wrong
        NSLog(@"Error");
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Something went wrong" message:@"Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
        
    }];
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}


-(void) touchChanged
{
    //_touches = [DatabaseManager.instance touchesForClient:_client];
    [self refreshTable];
    //[self.theTableView reloadData];
}
-(void)reloadTable
{
    _touches = (NSMutableArray *) [DatabaseManager.instance touchesForClient:_client];
    [self.theTableView reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (IBAction)backAction:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
}

-(int)countOfGrays
{
    int counter=0;
    for(Touch *touch in _touches)
    {
        if(touch.currentColor==Gray)
        {
            counter++;
        }
    }
    return counter;
}

- (IBAction)unwindToTouchList:(UIStoryboardSegue *)unwindSegue
{
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"seeTouchDetailSegue"])
    {
        
        
        
        NSIndexPath * indexPath = _selectedIndexPath;
        TouchDetailVC *touchDetailVC = (TouchDetailVC*)segue.destinationViewController;
        touchDetailVC.client = [[_touches objectAtIndex:indexPath.row] client];
        touchDetailVC.touch = [_touches objectAtIndex:indexPath.row];
        touchDetailVC.touchNumber = (int)indexPath.row+1;
    }
    else if([segue.identifier isEqualToString:@"unwindToTouchListSegue"])
    {
        if([sender isKindOfClass:[UIButton class]]) {
            //NSIndexPath * indexPath = [self.theTableView indexPathForCell:(TouchListCell*)[[[sender superview]superview]superview]];
            ClientListTVC *clientListTVC = (ClientListTVC*)segue.destinationViewController;
            [clientListTVC.theTableView reloadData];
            
        }
    }
    //unwindToTouchListSegue
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
- (IBAction)addTouchAction:(id)sender
{
    Touch *touch = [Touch new];
    touch.client = _client;
    touch.type = 0;
    touch.date = [NSDate date];
    touch.name = @"To complete";
    touch.status = @"A";
    touch.note = @" ";
    
    
    MBProgressHUD *mbProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    mbProgressHUD.labelText = @"Creating touch...";
    
    [RequestManager registerOrUpdateTouch:touch withSuccessBlock:^(Touch *touch) {
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self loadData];
        [self.theTableView reloadData];
    } failureBlock:^{
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSLog(@"Failure");
    }];
}

- (IBAction)onboardButtonAction:(id)sender
{
    _client.status = @"CW";
    MBProgressHUD *mbProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    mbProgressHUD.labelText = @"Updating information";
    [RequestManager registerOrUpdateClient:_client withSuccessBlock:^(Client *client) {
        //
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self performSegueWithIdentifier:@"unwindToTouchListSegue" sender:self];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Client onboarded" message:@"Client updated succesfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [self reloadTable];
    } failureBlock:^{
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

- (IBAction)puntButtonAction:(id)sender {
    _client.status = @"CL";
    MBProgressHUD *mbProgressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    mbProgressHUD.labelText = @"Updating information";
    [RequestManager registerOrUpdateClient:_client withSuccessBlock:^(Client *client) {
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self performSegueWithIdentifier:@"unwindToTouchListSegue" sender:self];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Client punted" message:@"Client updated succesfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [self reloadTable];
    } failureBlock:^{
        //
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

- (IBAction)seeTouchDetails:(id)sender
{
    UIButton *senderButton = (UIButton*)sender;
    
    /*
    NSLog(@"Superview: %@",senderButton.superview.superview.superview.superview);
    _selectedIndexPath = [(UITableView *)senderButton.superview.superview.superview.superview.superview.superview indexPathForCell:(UITableViewCell *)senderButton.superview.superview.superview.superview];*/
    
    _selectedIndexPath = [NSIndexPath indexPathForItem:senderButton.tag inSection:0];
    
    [self performSegueWithIdentifier:@"seeTouchDetailSegue" sender:self];
}
@end
