//
//  LoginVC.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 27/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "LoginVC.h"

@interface LoginVC ()

@end

@implementation LoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"username"] )
    {
        _userNameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
        _passwordTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
        [self loginAction:self];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
/*
 [RequestManager allActiveClientsSuccessBLock:^(NSArray *schools) {
 //
 } failureBlock:^{
 //
 }];
 */
- (IBAction)loginAction:(id)sender {
    
    if((_userNameTextField.text.length!=0)&&(_passwordTextField.text.length!=0))
    {
        _errorMessageLabel.text = @"";
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [RequestManager loginForUser:_userNameTextField.text password:_passwordTextField.text andCompletionBlock:^(BOOL success, NSString *errorDescription, NSError *error) {
            //
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if(success)
            {
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"loggedIn"];
                [self performSegueWithIdentifier:@"loginSegue" sender:self];
                _errorMessageLabel.text = @"";
            }
            else
            {
                                
                if(error)
                _errorMessageLabel.text = @"Check your connection";
                else
                _errorMessageLabel.text = @"Wrong username or password";
            }
            
        }
            ];
    }
    else
    {
        _errorMessageLabel.text = @"enter your user/password";
    }
    

}
- (IBAction)forgotYourPasswordAction:(id)sender
{
    
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}
- (IBAction)unwindToLogin:(UIStoryboardSegue *)unwindSegue
{
    
   
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"" sender:self
     ];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _userNameTextField) {
        [_passwordTextField becomeFirstResponder];
    }
    else if (textField == _passwordTextField) {
        [_passwordTextField resignFirstResponder];
        [self loginAction:self];
    }
    
    return YES;
}

@end
