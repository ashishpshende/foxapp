//
//  ClientDetailVC.m
//  Pacific Life
//
//  Created by Edgard Aguirre Rozo on 30/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import "ClientDetailVC.h"

@interface ClientDetailVC ()

@end

@implementation ClientDetailVC
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadClientInfo];
    // Do any additional setup after loading the view.
}

-(void)loadClientInfo
{
    
    _nameLargeLabel.text = _client.name;
    _ratingLabel.text = _client.rating;
    _nameLabel.text = _client.name;
    _companyLabel.text = _client.company;
    _emailLabel.text = _client.email;
    _phoneLabel.text = _client.phone;
_addressLabel.text = _client.address;
    _cityLabel.text = _client.city;
    _stateLabel.text = _client.state;
    _zipLabel.text = _client.zip;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"touchesByClientSegue"])
    {
        if([sender isKindOfClass:[UIButton class]]) {
            TouchListTVC *touchListVC = (TouchListTVC*)segue.destinationViewController;
            touchListVC.client = _client;
        }
        
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
@end
