//
//  Pacific_LifeTests.m
//  Pacific LifeTests
//
//  Created by Edgard Aguirre Rozo on 14/05/14.
//  Copyright (c) 2014 Globant. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Pacific_LifeTests : XCTestCase

@end

@implementation Pacific_LifeTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
